<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AccountItemWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Пішіні</translation>
    </message>
    <message>
        <source>Tom Hardy</source>
        <translation type="obsolete">Том Харди</translation>
    </message>
    <message>
        <source>Create new user</source>
        <translation type="obsolete">Жаңа пайдаланушыны құру</translation>
    </message>
    <message>
        <source>disable</source>
        <translation type="obsolete">Жарамсыз ету</translation>
    </message>
    <message>
        <source>enable</source>
        <translation type="obsolete">&amp; Қосу</translation>
    </message>
</context>
<context>
    <name>AccountSubItem</name>
    <message>
        <location filename="../../plugins/account/account-subitem.cpp" line="36"/>
        <source>account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/account-subitem.cpp" line="62"/>
        <source>New User</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountWidget</name>
    <message>
        <location filename="../../plugins/account/account-widget.cpp" line="100"/>
        <location filename="../../plugins/account/account-widget.cpp" line="420"/>
        <source>disable</source>
        <translation type="unfinished">Жарамсыз ету</translation>
    </message>
    <message>
        <location filename="../../plugins/account/account-widget.cpp" line="100"/>
        <location filename="../../plugins/account/account-widget.cpp" line="420"/>
        <source>enable</source>
        <translation type="unfinished">&amp; Қосу</translation>
    </message>
    <message>
        <location filename="../../plugins/account/account-widget.cpp" line="225"/>
        <source>Create new user</source>
        <translation type="unfinished">Жаңа пайдаланушыны құру</translation>
    </message>
</context>
<context>
    <name>AdvanceSettings</name>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="14"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="215"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="61"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="216"/>
        <source>Login shell</source>
        <translation type="unfinished">Кіру қабығы</translation>
    </message>
    <message>
        <source>Specify user id</source>
        <translation type="obsolete">Пайдаланушы идентификаторын көрсетіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="93"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="218"/>
        <source>EditLoginShell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="110"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="220"/>
        <source>Specify user id (needs to be greater than 1000)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="165"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="222"/>
        <source>EditSpecifyUserID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="183"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="224"/>
        <source>Specify user home</source>
        <translation type="unfinished">Пайдаланушының басты бетін көрсетіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="228"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="226"/>
        <source>EditSpecifyUserHome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="289"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="229"/>
        <source>ButtonConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="292"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="231"/>
        <source>confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="333"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="233"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.ui" line="336"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_advance-settings.h" line="235"/>
        <source>cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="133"/>
        <source>Advance Settings</source>
        <translation type="unfinished">Қосымша баптаулары</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="151"/>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="161"/>
        <source>Automatically generated by system</source>
        <translation type="unfinished">Автоматты жүйені құру</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="192"/>
        <source>Please enter the correct path</source>
        <translation type="unfinished">Дұрыс жолды енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="201"/>
        <source>Please enter specify user Id</source>
        <translation type="unfinished">Көрсетілген пайдаланушы идентификаторын енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="207"/>
        <source>Please enter an integer above 1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="216"/>
        <source>Please enter the correct home directory</source>
        <translation type="unfinished">Дұрыс үй каталогын енгізіңіз</translation>
    </message>
</context>
<context>
    <name>AppearancePlugin</name>
    <message>
        <location filename="../../plugins/appearance/appearance-plugin.cpp" line="67"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/appearance-plugin.cpp" line="74"/>
        <source>Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/appearance-plugin.cpp" line="81"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSystemTray</name>
    <message>
        <location filename="../../plugins/audio/src/system-tray/audio-system-tray.cpp" line="95"/>
        <source>Volume Setting</source>
        <translation type="unfinished">Дыбыс деңгейі</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/system-tray/audio-system-tray.cpp" line="96"/>
        <source>Mixed Setting</source>
        <translation type="unfinished">Араластыру параметрлері</translation>
    </message>
</context>
<context>
    <name>AuthManagerPage</name>
    <message>
        <source>AuthManagerPage</source>
        <translation type="obsolete">AuthManagerPage</translation>
    </message>
    <message>
        <source>Fingerprint Authentication</source>
        <translation type="obsolete">Саусақ ізін аутентификациялау</translation>
    </message>
    <message>
        <source>Face Authentication</source>
        <translation type="obsolete">Бет аутентификациясы</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="obsolete">Парольді тексеру</translation>
    </message>
    <message>
        <source>save</source>
        <translation type="obsolete">&amp; Сақтау</translation>
    </message>
    <message>
        <source>return</source>
        <translation type="obsolete">Қайтару</translation>
    </message>
    <message>
        <source>add fingerprint</source>
        <translation type="obsolete">Adfingerprint</translation>
    </message>
    <message>
        <source>add face</source>
        <translation type="obsolete">Кеспе қосу</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="obsolete">Қате</translation>
    </message>
    <message>
        <source>please ensure that at least one authentication option exists</source>
        <translation type="obsolete">Кем дегенде бір аутентификация опциясы бар екеніне көз жеткізіңіз</translation>
    </message>
    <message>
        <source>fingerprint_</source>
        <translation type="obsolete">Саусақ ізі_</translation>
    </message>
    <message>
        <source>face_</source>
        <translation type="obsolete">&amp; Беткі</translation>
    </message>
</context>
<context>
    <name>AuthPlugin</name>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="91"/>
        <source>Fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="98"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="105"/>
        <source>UKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="112"/>
        <source>Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="119"/>
        <source>Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="126"/>
        <source>Driver Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/auth-plugin.cpp" line="133"/>
        <source>Prefs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BatterySettingsPage</name>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="14"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="195"/>
        <source>BatterySettingsPage</source>
        <translation type="unfinished">Батареяны баптау беті</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="43"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="196"/>
        <source>After idle for more than the following time, the computer will execute</source>
        <translation type="unfinished">Компьютер келесі уақыттан артық жұмыс істемей тұрғаннан кейін жұмыс істейді</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="60"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="198"/>
        <source>ComboIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="67"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="201"/>
        <source>ComboIdleAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="87"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="206"/>
        <source>When the battery is lit up, it will be executed</source>
        <translation type="unfinished">Батарея жанған кезде ол іске қосылады</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="104"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="208"/>
        <source>ComboLowBatteryAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="121"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="210"/>
        <source>The monitor will turn off when it is idle</source>
        <translation type="unfinished">Монитор жұмыс істемей тұрғанда өшіріледі</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="138"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="212"/>
        <source>ComboMonitorTurnOffIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="155"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="214"/>
        <source>Reduce screen brightness when idle</source>
        <translation type="unfinished">Бос тұрған кезде экранның жарықтығын азайтыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="182"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="215"/>
        <source>Reduce screen brightness when  no power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.ui" line="212"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_battery-settings-page.h" line="216"/>
        <source>The energy saving mode is enabled when the power is low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display Off</source>
        <translation type="obsolete">Дисплейдің жабылуы</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="59"/>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="75"/>
        <source>Suspend</source>
        <translation type="unfinished">&amp; Кідірту</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="60"/>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="76"/>
        <source>Shutdown</source>
        <translation type="unfinished">Жабу</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="61"/>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="77"/>
        <source>Hibernate</source>
        <translation type="unfinished">Ұйқы</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="62"/>
        <location filename="../../plugins/power/pages/battery-settings-page.cpp" line="78"/>
        <source>Do nothing</source>
        <translation type="unfinished">Ештеңе жасамаңыз</translation>
    </message>
</context>
<context>
    <name>BatterySubItem</name>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Батарея параметрлері</translation>
    </message>
</context>
<context>
    <name>BiometricItem</name>
    <message>
        <source>BiometricItem</source>
        <translation type="obsolete">BiometricItem</translation>
    </message>
    <message>
        <source>text</source>
        <translation type="obsolete">Мәтін</translation>
    </message>
    <message>
        <source>add</source>
        <translation type="obsolete">&amp; Плюс</translation>
    </message>
</context>
<context>
    <name>CPanelAudioWidget</name>
    <message>
        <location filename="../../plugins/audio/src/plugin/cpanel-audio-widget.ui" line="14"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_cpanel-audio-widget.h" line="118"/>
        <source>CPanelAudioWidget</source>
        <translation type="unfinished">CPanelAudioWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/cpanel-audio-widget.cpp" line="40"/>
        <source>Output</source>
        <translation type="unfinished">Шығу</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/cpanel-audio-widget.cpp" line="41"/>
        <source>Input</source>
        <translation type="unfinished">Енгізу</translation>
    </message>
</context>
<context>
    <name>CPanelNetworkWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.ui" line="20"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_cpanel-network-widget.h" line="96"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_cpanel-network-widget.h" line="96"/>
        <source>CPanelNetworkWidget</source>
        <translation type="unfinished">CPanelNetworkWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="291"/>
        <source>Wireless Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="111"/>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="115"/>
        <source>VPN</source>
        <translation type="unfinished">Виртуалды жеке желі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="120"/>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="123"/>
        <source>Network Details</source>
        <translation type="unfinished">Желі мәліметтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="229"/>
        <source>Connected</source>
        <translation type="unfinished">Қосылған</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="233"/>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/cpanel-network-widget.cpp" line="237"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CPanelPowerWidget</name>
    <message>
        <source>CPanelPowerWidget</source>
        <translation type="obsolete">CPanelPowerWidget</translation>
    </message>
    <message>
        <source>General Settings</source>
        <translation type="obsolete">Жалпы баптаулары</translation>
    </message>
    <message>
        <source>Power Settings</source>
        <translation type="obsolete">Қуат параметрлері</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Батарея параметрлері</translation>
    </message>
</context>
<context>
    <name>ChangeHostNameWidget</name>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="32"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="144"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="107"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="145"/>
        <source>Host Name:</source>
        <translation type="unfinished">Хосттың атауы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="132"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="147"/>
        <source>EditHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="185"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="150"/>
        <source>ButtonSaveHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="188"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="152"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="223"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="154"/>
        <source>ButtonCancelChangeHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.ui" line="226"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_change-host-name-widget.h" line="156"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <source>Host Name</source>
        <translation type="obsolete">Хост атауы</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.cpp" line="72"/>
        <source>Warning</source>
        <translation type="unfinished">Ескерту</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/change-host-name-widget.cpp" line="73"/>
        <source>Change host name failed! Please check the Dbus service!</source>
        <translation type="unfinished">Хост атауын өзгерту сәтсіз аяқталды! Dbus қызметін тексеріңіз!</translation>
    </message>
</context>
<context>
    <name>CheckpasswdDialog</name>
    <message>
        <location filename="../../plugins/authentication/checkpasswd-dialog.cpp" line="96"/>
        <source>Check password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/checkpasswd-dialog.cpp" line="97"/>
        <source>Check the current password before you enroll the feature</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChooseItem</name>
    <message>
        <location filename="../../plugins/keyboard/utils/choose-item.ui" line="35"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_choose-item.h" line="80"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>ConnectionDetailsWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="487"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="487"/>
        <source>ConnectionDetailsWidget</source>
        <translation type="unfinished">ConnectionDetailsWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="89"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="488"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="488"/>
        <source>Security type</source>
        <translation type="unfinished">Қауіпсіздік түрі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="109"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="175"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="241"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="315"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="381"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="447"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="513"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="579"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="645"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="711"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="777"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="843"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="909"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="489"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="491"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="493"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="495"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="497"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="499"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="501"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="503"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="505"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="507"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="509"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="511"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="513"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="489"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="491"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="493"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="495"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="497"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="499"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="501"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="503"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="505"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="507"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="509"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="511"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="513"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="155"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="490"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="490"/>
        <source>Frequency band</source>
        <translation type="unfinished">Жиілік диапазоны</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="221"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="492"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="492"/>
        <source>Channel</source>
        <translation type="unfinished">Арна</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="295"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="494"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="494"/>
        <source>Interface</source>
        <translation type="unfinished">Интерфейс</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="361"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="496"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="496"/>
        <source>MAC</source>
        <translation type="unfinished">Алма</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="427"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="498"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="498"/>
        <source>IPv4</source>
        <translation type="unfinished">IPV4</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="493"/>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="757"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="500"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="508"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="500"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="508"/>
        <source>Gateway</source>
        <translation type="unfinished">Шлюз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="559"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="502"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="502"/>
        <source>DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferred DNS</source>
        <translation type="obsolete">Қалаған DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="625"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="504"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="504"/>
        <source>Subnet mask</source>
        <translation type="unfinished">Ішкі желі маскасы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="691"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="506"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="506"/>
        <source>IPv6</source>
        <translation type="unfinished">IPV6</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="823"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="510"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="510"/>
        <source>Prefix</source>
        <translation type="unfinished">Префиксі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="889"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-details-widget.h" line="512"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-details-widget.h" line="512"/>
        <source>Rate</source>
        <translation type="unfinished">Бағасы</translation>
    </message>
</context>
<context>
    <name>ConnectionLists</name>
    <message>
        <source>Tips</source>
        <translation type="obsolete">Кеңестер</translation>
    </message>
    <message>
        <source>Please input a network name</source>
        <translation type="obsolete">Желі атауын енгізіңіз</translation>
    </message>
    <message>
        <source>Other WiFi networks</source>
        <translation type="obsolete">Басқа WiFi желілері</translation>
    </message>
</context>
<context>
    <name>ConnectionNameWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-name-widget.h" line="90"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-name-widget.h" line="90"/>
        <source>ConnectionNameWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="58"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-name-widget.h" line="91"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-name-widget.h" line="91"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="77"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-name-widget.h" line="93"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-name-widget.h" line="93"/>
        <source>EditConnectionName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="88"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-name-widget.h" line="95"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-name-widget.h" line="95"/>
        <source>Auto Connection</source>
        <translation type="unfinished">Автоматты түрде қосылыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="43"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="84"/>
        <source>Wired Connection %1</source>
        <translation type="unfinished">% 1 сымды қосылым</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="123"/>
        <source>VPN L2TP %1</source>
        <translation type="unfinished">VPN L2TP% 1</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="126"/>
        <source>VPN PPTP %1</source>
        <translation type="unfinished">VPN PPTP% 1</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="187"/>
        <source>Connection name can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectionShowPage</name>
    <message>
        <location filename="../../plugins/network/src/plugin/connection-show-page.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-show-page.h" line="99"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-show-page.h" line="99"/>
        <source>ConnectionShowPage</source>
        <translation type="unfinished">Қосылымды көрсету беті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/connection-show-page.ui" line="62"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-show-page.h" line="100"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-show-page.h" line="100"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/connection-show-page.ui" line="93"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_connection-show-page.h" line="102"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_connection-show-page.h" line="102"/>
        <source>ButtonCreateConnection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="obsolete">Жасау</translation>
    </message>
</context>
<context>
    <name>CreateGroupPage</name>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.ui" line="32"/>
        <source>CreateGroupPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.ui" line="95"/>
        <source>Create Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.ui" line="166"/>
        <source>Add Group Members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.ui" line="246"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="101"/>
        <source>Please enter your group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="118"/>
        <source>group name cannot be a pure number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="125"/>
        <source>group name already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="153"/>
        <location filename="../../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="172"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
</context>
<context>
    <name>CreateUserPage</name>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="14"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="286"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="74"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="288"/>
        <source>UserAvatarWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="104"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="290"/>
        <source>User name</source>
        <translation type="unfinished">Пайдаланушы аты</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="136"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="292"/>
        <source>EditUserName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="153"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="294"/>
        <source>User type</source>
        <translation type="unfinished">Пайдаланушы түрі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="182"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="296"/>
        <source>ComboUserType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="199"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="298"/>
        <source>Password</source>
        <translation type="unfinished">Пароль</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="234"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="300"/>
        <source>EditPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="251"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="302"/>
        <source>Confirm password</source>
        <translation type="unfinished">Парольді растау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="286"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="304"/>
        <source>EditPasswdConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="306"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="307"/>
        <source>ButtonAdvanceSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="312"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="309"/>
        <source>Advance setting</source>
        <translation type="unfinished">Алдын ала орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="407"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="311"/>
        <source>ButtonConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="410"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="313"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="457"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="315"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.ui" line="460"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_create-user-page.h" line="317"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="83"/>
        <source>standard</source>
        <translation type="unfinished">Стандартты</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="84"/>
        <source>administrator</source>
        <translation type="unfinished">Администратор</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="107"/>
        <source>Please enter user name first</source>
        <translation type="unfinished">Алдымен пайдаланушы атын енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="133"/>
        <source>Please enter your user name</source>
        <translation type="unfinished">Пайдаланушы атыңызды енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="149"/>
        <source>user name cannot be a pure number</source>
        <translation type="unfinished">Пайдаланушы аты таза сан бола алмайды</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="156"/>
        <source>user name already exists</source>
        <translation type="unfinished">Пайдаланушы аты бұрыннан бар</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="168"/>
        <source>Please enter your password</source>
        <translation type="unfinished">Құпия сөзді енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="175"/>
        <source>Please enter the password again</source>
        <translation type="unfinished">Құпия сөзді қайта енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="182"/>
        <source>The password you enter must be the same as the former one</source>
        <translation type="unfinished">Сіз енгізген пароль алдыңғы парольмен бірдей болуы керек</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="192"/>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="227"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/create-user-page/create-user-page.cpp" line="192"/>
        <source>Password encryption failed</source>
        <translation type="unfinished">Парольді шифрлау жаңылысы</translation>
    </message>
</context>
<context>
    <name>CursorThemePage</name>
    <message>
        <location filename="../../plugins/appearance/pages/theme/cursor/cursor-theme-page.cpp" line="55"/>
        <source>Cursor Themes Settings</source>
        <translation type="unfinished">Меңзер нақышын баптауName</translation>
    </message>
</context>
<context>
    <name>CursorThemes</name>
    <message>
        <source>Cursor Themes Settings</source>
        <translation type="obsolete">Меңзер нақышын баптауName</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="obsolete">&amp; Жетіспеу</translation>
    </message>
    <message>
        <source>Set cursor themes failed!</source>
        <translation type="obsolete">Меңзер нақышын орнату сәтсіз аяқталды!</translation>
    </message>
</context>
<context>
    <name>DateTimeSettings</name>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="148"/>
        <source>DateTimeSettings</source>
        <translation type="unfinished">Күн уақытының параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="46"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="149"/>
        <source>Select Time</source>
        <translation type="unfinished">&amp; Уақытты таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="88"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="150"/>
        <source>Select Date</source>
        <translation type="unfinished">Күнін таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="152"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="152"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="155"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="154"/>
        <source>save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="196"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="156"/>
        <source>ButtonReset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="199"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_date-time-settings.h" line="158"/>
        <source>reset</source>
        <translation type="unfinished">Қалпына келтіру</translation>
    </message>
</context>
<context>
    <name>DaySpinBox</name>
    <message>
        <location filename="../../plugins/timedate/widgets/date-spinbox.h" line="65"/>
        <source>%1</source>
        <translation type="unfinished">%1</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="14"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="188"/>
        <source>DefaultApp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="50"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="189"/>
        <source>Web Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="84"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="190"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="115"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="191"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="146"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="192"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="177"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="193"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/application/src/pages/defaultapp.ui" line="208"/>
        <location filename="../../build/plugins/application/kiran-cpanel-defaultapp_autogen/include/ui_defaultapp.h" line="194"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DefaultappPlugin</name>
    <message>
        <location filename="../../plugins/application/src/defaultapp-plugin.cpp" line="66"/>
        <location filename="../../plugins/application/src/defaultapp-plugin.cpp" line="73"/>
        <source>DefaultApp</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPage</name>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/details-page.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_details-page.h" line="118"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_details-page.h" line="118"/>
        <source>DetailsPage</source>
        <translation type="unfinished">Мәліметтер парағы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/details-page.ui" line="62"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_details-page.h" line="119"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_details-page.h" line="119"/>
        <source>Network Details</source>
        <translation type="unfinished">Желі мәліметтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/details-page.ui" line="121"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_details-page.h" line="120"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_details-page.h" line="120"/>
        <source>Please select a connection</source>
        <translation type="unfinished">Қосылымды таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/details-page/details-page.ui" line="140"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_details-page.h" line="122"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_details-page.h" line="122"/>
        <source>ComboBoxDetailsSelectConnection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="14"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="145"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="122"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="147"/>
        <source>Rotate left 90 degrees</source>
        <translation type="unfinished">90 градусқа солға бұру</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="125"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="150"/>
        <source>ButtonLeft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="156"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="154"/>
        <source>Rotate right 90 degrees</source>
        <translation type="unfinished">90 градусқа оңға бұрыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="159"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="157"/>
        <source>ButtonRight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="190"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="161"/>
        <source>Turn left and right</source>
        <translation type="unfinished">Солға оңға бұрылыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="193"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="164"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="227"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="168"/>
        <source>upside down</source>
        <translation type="unfinished">Төменге қарай</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="230"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="171"/>
        <source>ButtonVertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="264"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="175"/>
        <source>Identification display</source>
        <translation type="unfinished">Тану дисплейі</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/device-panel.ui" line="267"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_device-panel.h" line="178"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisconnectAndDeleteButton</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="60"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="60"/>
        <source>DisconnectAndDeleteButton</source>
        <translation type="unfinished">DisconnectAndDeleteButton</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="35"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="62"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="62"/>
        <source>ButtonDisconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="38"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="64"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="64"/>
        <source>Disconnect</source>
        <translation type="unfinished">Ажырату</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="45"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="66"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="66"/>
        <source>ButtonDelete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="48"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="68"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="68"/>
        <source>Delete</source>
        <translation type="unfinished">Жою</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="55"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="70"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="70"/>
        <source>ButtonIgnore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="58"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_disconnect-and-delete-button.h" line="72"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_disconnect-and-delete-button.h" line="72"/>
        <source>Ignore</source>
        <translation type="unfinished">Елемеу</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.cpp" line="100"/>
        <source>Are you sure you want to delete the connection %1</source>
        <translation type="unfinished">% 1 қосылымын өшіргіңіз келгені рас</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.cpp" line="101"/>
        <source>Warning</source>
        <translation type="unfinished">Ескерту</translation>
    </message>
</context>
<context>
    <name>DisplayFormatSettings</name>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="128"/>
        <source>DisplayFormatSettings</source>
        <translation type="unfinished">IsplayorMattings</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="46"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="129"/>
        <source>Long date display format</source>
        <translation type="unfinished">Ұзақ күнді көрсету пішімі</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="55"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="131"/>
        <source>ComboLongDateDisplayFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="73"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="133"/>
        <source>Short date display format</source>
        <translation type="unfinished">Қысқа күнді көрсету пішімі</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="82"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="135"/>
        <source>ComboShortDateDisplayFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="97"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="137"/>
        <source>Time format</source>
        <translation type="unfinished">Уақыт пішімі</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="106"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="139"/>
        <source>ComboTimeFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="124"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_display-format-settings.h" line="141"/>
        <source>Show time witch seconds</source>
        <translation type="unfinished">Уақыт сиқыршысының секундтарын көрсету</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.cpp" line="64"/>
        <source>24-hours</source>
        <translation type="unfinished">24 сағат</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/display-format-settings/display-format-settings.cpp" line="65"/>
        <source>12-hours</source>
        <translation type="unfinished">12 сағат</translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="14"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="473"/>
        <source>DisplayPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="110"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="475"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="134"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="477"/>
        <source>Copy display</source>
        <translation type="unfinished">Дисплейді көшіру</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="162"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="479"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="186"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="481"/>
        <source>Extended display</source>
        <translation type="unfinished">Кеңейтілген дисплей</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="289"/>
        <location filename="../../plugins/display/src/display-page.ui" line="562"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="482"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="503"/>
        <source>Resolution ratio</source>
        <translation type="unfinished">Ажыратымдылығы</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="308"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="484"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="325"/>
        <location filename="../../plugins/display/src/display-page.ui" line="598"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="486"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="507"/>
        <source>Refresh rate</source>
        <translation type="unfinished">Жаңарту жиілігі</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="344"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="488"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="358"/>
        <location filename="../../plugins/display/src/display-page.ui" line="631"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="490"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="511"/>
        <source>Zoom rate</source>
        <translation type="unfinished">Масштабтау жылдамдығы</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="377"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="496"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="381"/>
        <location filename="../../plugins/display/src/display-page.ui" line="654"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="491"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="512"/>
        <source>Automatic</source>
        <translation type="unfinished">Автоматты</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="386"/>
        <location filename="../../plugins/display/src/display-page.ui" line="659"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="492"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="513"/>
        <source>100% (recommended)</source>
        <translation type="unfinished">100% (ұсынылады)</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="391"/>
        <location filename="../../plugins/display/src/display-page.ui" line="664"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="493"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="514"/>
        <source>200%</source>
        <translation type="unfinished">200%</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="462"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="498"/>
        <source>Open</source>
        <translation type="unfinished">Ашу</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="514"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="499"/>
        <source>Set as main display</source>
        <translation type="unfinished">Негізгі дисплейді орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="540"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="501"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="581"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="505"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="617"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="509"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="650"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="517"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="731"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="520"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="737"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="522"/>
        <source>Apply</source>
        <translation type="unfinished">Қолдану</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="756"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="524"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.ui" line="762"/>
        <location filename="../../build/plugins/display/kiran-cpanel-display_autogen/include/ui_display-page.h" line="526"/>
        <source>Close</source>
        <translation type="unfinished">Жабу</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="255"/>
        <location filename="../../plugins/display/src/display-page.cpp" line="273"/>
        <source> (recommended)</source>
        <translation type="unfinished">(Ұсынылады)</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="364"/>
        <source>Is the display normal?</source>
        <translation type="unfinished">Бұл қалыпты ма?</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="367"/>
        <source>Save current configuration(K)</source>
        <translation type="unfinished">Назардағы конфигурацияны сақтау (K)</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="371"/>
        <source>Restore previous configuration(R)</source>
        <translation type="unfinished">Алдыңғы конфигурацияны қалпына келтіру (R)</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="379"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation type="unfinished">Дисплей% 1 секундтан кейін алдыңғы конфигурацияны қалпына келтіреді</translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../../plugins/display/src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DnsWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dns-widget.h" line="66"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dns-widget.h" line="66"/>
        <source>DnsWidget</source>
        <translation type="unfinished">DnsWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="32"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dns-widget.h" line="67"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dns-widget.h" line="67"/>
        <source>Preferred DNS</source>
        <translation type="unfinished">Қалаған DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="42"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dns-widget.h" line="68"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dns-widget.h" line="68"/>
        <source>Alternate DNS</source>
        <translation type="unfinished">Күту режиміндегі DNS</translation>
    </message>
</context>
<context>
    <name>DriverPage</name>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="43"/>
        <source>device type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="55"/>
        <source>driver list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="66"/>
        <source>Fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="67"/>
        <source>Fingervein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="68"/>
        <source>iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="69"/>
        <source>ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/driver-page.cpp" line="70"/>
        <source>face</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DslManager</name>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/dsl-manager.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dsl-manager.h" line="76"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dsl-manager.h" line="76"/>
        <source>DslManager</source>
        <translation type="unfinished">DslManager</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/dsl-manager.cpp" line="33"/>
        <source>DSL</source>
        <translation type="unfinished">DSL</translation>
    </message>
</context>
<context>
    <name>DslSettingPage</name>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dsl-setting-page.h" line="99"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dsl-setting-page.h" line="99"/>
        <source>DslSettingPage</source>
        <translation type="unfinished">DslSettingPage</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="72"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dsl-setting-page.h" line="100"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dsl-setting-page.h" line="100"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="92"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_dsl-setting-page.h" line="101"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_dsl-setting-page.h" line="101"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
</context>
<context>
    <name>EthernetWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="124"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="124"/>
        <source>EthernetWidget</source>
        <translation type="unfinished">EthernetWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="40"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="125"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="125"/>
        <source>MAC Address Of Ethernet Device</source>
        <translation type="unfinished">Ethernet құрылғысының MAC адресі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="59"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="127"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="127"/>
        <source>ComboBoxDeviceMac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="76"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="129"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="129"/>
        <source>Ethernet Clone MAC Address</source>
        <translation type="unfinished">Ethernet клонының MAC мекенжайы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="95"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="131"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="131"/>
        <source>EditDeviceMac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="117"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="133"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="133"/>
        <source>Custom MTU</source>
        <translation type="unfinished">Қалаған MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="151"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ethernet-widget.h" line="135"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ethernet-widget.h" line="135"/>
        <source>SpinBoxCustomMTU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.cpp" line="45"/>
        <source>No device specified</source>
        <translation type="unfinished">Құрылғы көрсетілмеген</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ethernet-widget.cpp" line="165"/>
        <source>Clone Mac invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FaceEnrollDialog</name>
    <message>
        <source>FaceEnrollDialog</source>
        <translation type="obsolete">FaceEnrollDialog</translation>
    </message>
    <message>
        <source>balabalalbala...</source>
        <translation type="obsolete">Барбара Барабара...</translation>
    </message>
    <message>
        <source>save</source>
        <translation type="obsolete">&amp; Сақтау</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="obsolete">Бас тарту</translation>
    </message>
    <message>
        <source>initializing face collection environment...</source>
        <translation type="obsolete">Бет жинау ортасын баптау...</translation>
    </message>
    <message>
        <source>failed to initialize face collection environment!</source>
        <translation type="obsolete">Бет жинау ортасын баптау мүмкін емес!</translation>
    </message>
    <message>
        <source>Failed to start collection</source>
        <translation type="obsolete">Жинауды бастау мүмкін емес</translation>
    </message>
</context>
<context>
    <name>FacePage</name>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="65"/>
        <source>face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="66"/>
        <source>Default face device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="67"/>
        <source>face feature list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="99"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="147"/>
        <source>Start enroll failed,%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="148"/>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="179"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="173"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="174"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/face-page.cpp" line="178"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FingerPage</name>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="88"/>
        <source>fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="88"/>
        <source>fingervein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="90"/>
        <source>Default %1 device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="91"/>
        <source>%1 list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="122"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="150"/>
        <source>Start enroll failed,%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="151"/>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="204"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="198"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="199"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/finger-page.cpp" line="203"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FingerprintEnrollDialog</name>
    <message>
        <source>FingerprintEnrollDialog</source>
        <translation type="obsolete">Саусақ ізін айналдыру диалогы</translation>
    </message>
    <message>
        <source>balabalalbala...</source>
        <translation type="obsolete">Барбара Барабара...</translation>
    </message>
    <message>
        <source>save</source>
        <translation type="obsolete">&amp; Сақтау</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="obsolete">Бас тарту</translation>
    </message>
    <message>
        <source>Finger Enroll</source>
        <translation type="obsolete">Саусақтарды тіркеу</translation>
    </message>
    <message>
        <source>This fingerprint is bound to the user(%1)</source>
        <translation type="obsolete">Бұл саусақ ізі пайдаланушыға байланған (% 1)</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Ақпарат</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Қате</translation>
    </message>
</context>
<context>
    <name>FingerprintInputWorker</name>
    <message>
        <source>initializing fingerprint collection environment...</source>
        <translation type="obsolete">Саусақ ізін жинау ортасын баптау...</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="14"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="193"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="76"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="194"/>
        <source>Application Font Settings</source>
        <translation type="unfinished">Қолданбаның қаріп параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="100"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="196"/>
        <source>ComboAppFontName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="125"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="199"/>
        <source>ComboAppFontSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="158"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="201"/>
        <source>Titlebar Font Settings</source>
        <translation type="unfinished">Тақырып жолағының қаріп параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="182"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="203"/>
        <source>ComboTitleFontName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="201"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="206"/>
        <source>ComboTitleFontSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="234"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="208"/>
        <source>Monospace Font Settings</source>
        <translation type="unfinished">Қаріптің тұрақты ені</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="258"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="210"/>
        <source>ComboMonospaceFontName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.ui" line="277"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_fonts.h" line="213"/>
        <source>ComboMonospaceFontSize</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralBioPage</name>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="156"/>
        <source>Rename Feature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="157"/>
        <source>Please enter the renamed feature name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="186"/>
        <source>Are you sure you want to delete the feature called %1, Ensure that the Ukey device is inserted; otherwise the information stored in the Ukey will not be deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="193"/>
        <source>Are you sure you want to delete the feature called %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="196"/>
        <source>tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="220"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="221"/>
        <source> Failed to enroll feature because the password verification failed！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="235"/>
        <source>default device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/utils/general-bio-page.cpp" line="247"/>
        <source>feature list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralPage</name>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="14"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="277"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="55"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="278"/>
        <source>Capslock Tip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="86"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="280"/>
        <source>Numlock Tip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="123"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="282"/>
        <source>Repeat Key</source>
        <translation type="unfinished">Қайталау пернесі</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="130"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="283"/>
        <source>(Repeat a key while holding it down)</source>
        <translation type="unfinished">(Пернелерді басып тұрғанда қайталаңыз)</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="150"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="285"/>
        <source>SwitchRepeatKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="169"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="288"/>
        <source>Delay</source>
        <translation type="unfinished">Кешіктіру</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="191"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="290"/>
        <source>SliderRepeatDelay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="215"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="292"/>
        <source>Short</source>
        <translation type="unfinished">Қысқа</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="235"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="293"/>
        <source>Long</source>
        <translation type="unfinished">Ұзын</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="253"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="294"/>
        <source>Interval</source>
        <translation type="unfinished">Аралығы</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="275"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="296"/>
        <source>SliderRepeatInterval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="302"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="298"/>
        <source>Slow</source>
        <translation type="unfinished">Баяу</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="322"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="299"/>
        <source>Fast</source>
        <translation type="unfinished">Жылдам</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="338"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="300"/>
        <location filename="../../plugins/keyboard/pages/general-page.cpp" line="54"/>
        <source>Enter repeat characters to test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter characters to test the settings</source>
        <translation type="obsolete">Параметрлерді тексеру үшін таңбаларды енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/general-page.ui" line="357"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_general-page.h" line="302"/>
        <source>EditTestRepeatKey</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralSettingsPage</name>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="14"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="481"/>
        <source>GeneralSettingsPage</source>
        <translation type="unfinished">Жалпы параметрлер беті</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="104"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="482"/>
        <source>When the power button is pressed</source>
        <translation type="unfinished">Қуат түймесі басылған кезде</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="127"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="484"/>
        <source>ComboPowerButtonAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="171"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="486"/>
        <source>When the suspend button is pressed</source>
        <translation type="unfinished">Кідірту түймесі басылған кезде</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="194"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="488"/>
        <source>ComboSuspendAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="238"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="490"/>
        <source>When closing the lid</source>
        <translation type="unfinished">Қақпақты жапқанда</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="279"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="492"/>
        <source>ComboCloseLidAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="323"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="494"/>
        <source>Computer Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="394"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="495"/>
        <source>Display brightness setting</source>
        <translation type="unfinished">Дисплей жарықтығының параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="414"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="496"/>
        <source>0%</source>
        <translation type="unfinished">0%</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="444"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="498"/>
        <source>SliderDisplayBrightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="493"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="500"/>
        <source>Color temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="524"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="501"/>
        <source>Automatic color temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="595"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="502"/>
        <source>cold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="615"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="503"/>
        <source>standard</source>
        <translation type="unfinished">Стандартты</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="635"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="504"/>
        <source>warm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="695"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="508"/>
        <source>Regard computer as idle after</source>
        <translation type="unfinished">Компьютерді бос деп ойлаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="749"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="511"/>
        <source>SliderComputerIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="780"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="513"/>
        <source>Lock screen when idle</source>
        <translation type="unfinished">Бос тұрған кезде экранды құлыптау</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.ui" line="823"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_general-settings-page.h" line="514"/>
        <source>password is required to wake up in standby mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="121"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="134"/>
        <source>shutdown</source>
        <translation type="unfinished">Жабу</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="122"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="128"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="133"/>
        <source>hibernate</source>
        <translation type="unfinished">Ұйқы</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="123"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="127"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="132"/>
        <source>suspend</source>
        <translation type="unfinished">&amp; Кідірту</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="124"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="129"/>
        <source>display off</source>
        <translation type="unfinished">Дисплейдің жабылуы</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="125"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="130"/>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="135"/>
        <source>do nothing</source>
        <translation type="unfinished">Ештеңе жасамаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="414"/>
        <source>ERROR</source>
        <translation type="unfinished">ERROR</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="530"/>
        <source>%1hour</source>
        <translation type="unfinished">Amount in units (real)</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/general-settings-page.cpp" line="534"/>
        <source>%1minute</source>
        <translation type="unfinished">Amount in units (real)</translation>
    </message>
</context>
<context>
    <name>GeneralSettingsSubItem</name>
    <message>
        <source>General Settings</source>
        <translation type="obsolete">Жалпы баптаулары</translation>
    </message>
</context>
<context>
    <name>GeneralSubItem</name>
    <message>
        <location filename="../../plugins/keyboard/general-subitem.h" line="46"/>
        <source>Keyboard General Option</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralWidget</name>
    <message>
        <source>GeneralWidget</source>
        <translation type="obsolete">Әмбебап виджет</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">Мәтін тегтері</translation>
    </message>
    <message>
        <source>Auto Connection</source>
        <translation type="obsolete">Автоматты түрде қосылыңыз</translation>
    </message>
    <message>
        <source>Wired Connection %1</source>
        <translation type="obsolete">% 1 сымды қосылым</translation>
    </message>
    <message>
        <source>VPN L2TP %1</source>
        <translation type="obsolete">VPN L2TP% 1</translation>
    </message>
    <message>
        <source>VPN PPTP %1</source>
        <translation type="obsolete">VPN PPTP% 1</translation>
    </message>
</context>
<context>
    <name>GroupInfoPage</name>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="136"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="236"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="335"/>
        <source>Member List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="435"/>
        <source>Add User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="482"/>
        <source>Delete</source>
        <translation type="unfinished">Жою</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="564"/>
        <source>Add Member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="679"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.ui" line="726"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="123"/>
        <source>Please input keys for search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="247"/>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="259"/>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="269"/>
        <location filename="../../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="280"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
</context>
<context>
    <name>GroupSubItem</name>
    <message>
        <location filename="../../plugins/group/src/group-subitem.cpp" line="44"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/group-subitem.cpp" line="76"/>
        <source>Creat group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/group-subitem.cpp" line="77"/>
        <source>Change group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/group-subitem.cpp" line="78"/>
        <source>Add group member</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HardWorker</name>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="46"/>
        <source>Create User failed</source>
        <translation type="unfinished">Пайдаланушыны құру сәтсіз аяқталды</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="59"/>
        <source>Failed to connect to the account management service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="150"/>
        <source> update password failed</source>
        <translation type="unfinished">Парольді жаңарту жаңылысы</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="176"/>
        <source>icon file</source>
        <translation type="unfinished">&amp; Таңбаша файлы</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="187"/>
        <source>userName type</source>
        <translation type="unfinished">Пайдаланушы аты түрі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="198"/>
        <source>locked</source>
        <translation type="unfinished">Құлыпталған</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="208"/>
        <source>Failed to update user properties,%1</source>
        <translation type="unfinished">Пайдаланушы төлсипатын жаңарту жаңылысы,% 1</translation>
    </message>
    <message>
        <location filename="../../plugins/account/utils/hard-worker.cpp" line="230"/>
        <source>Failed to delete user,%1</source>
        <translation type="unfinished">% 1 пайдаланушысын өшіру жаңылысы</translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="38"/>
        <source>Create Group failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="92"/>
        <source>Failed to delete group,%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="113"/>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="135"/>
        <source> add user to group failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="158"/>
        <source> change group name failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/group/src/hard-worker.cpp" line="169"/>
        <source> change group name failed, the new group name is occupied</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HardwareInformation</name>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="14"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="266"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="147"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="267"/>
        <source>CPU:</source>
        <translation type="unfinished">Орталық өңдеу блогы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="167"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="269"/>
        <source>LabelCpuInfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="170"/>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="233"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="271"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="276"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="210"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="272"/>
        <source>Memory:</source>
        <translation type="unfinished">Жадтары:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="230"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="274"/>
        <source>LabelMemoryInfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="273"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="277"/>
        <source>Hard disk:</source>
        <translation type="unfinished">Қатты диск:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="351"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="278"/>
        <source>Graphics card:</source>
        <translation type="unfinished">Графикалық карта:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.ui" line="426"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_hardware-information.h" line="279"/>
        <source>Network card:</source>
        <translation type="unfinished">Желілік карта:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.cpp" line="109"/>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.cpp" line="110"/>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.cpp" line="118"/>
        <source>Unknow</source>
        <translation type="unfinished">Білмеймін</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/hardware-information/hardware-information.cpp" line="177"/>
        <source>%1 GB (%2 GB available)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HardwareSubItem</name>
    <message>
        <location filename="../../plugins/system/hardware-subitem.h" line="33"/>
        <source>Hardware Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconThemePage</name>
    <message>
        <location filename="../../plugins/appearance/pages/theme/icon/icon-theme-page.ui" line="14"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_icon-theme-page.h" line="83"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/icon/icon-theme-page.ui" line="35"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_icon-theme-page.h" line="84"/>
        <source>Icon Themes Setting</source>
        <translation type="unfinished">Таңбаша нақышын баптауName</translation>
    </message>
</context>
<context>
    <name>IconThemes</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Пішіні</translation>
    </message>
    <message>
        <source>Icon Themes Setting</source>
        <translation type="obsolete">Таңбаша нақышын баптауName</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="obsolete">&amp; Жетіспеу</translation>
    </message>
    <message>
        <source>Set icon themes failed!</source>
        <translation type="obsolete">Белгіше нақышын орнату сәтсіз аяқталды!</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="104"/>
        <source>Add Image Failed</source>
        <translation type="unfinished">Кескінді қосу жаңылысы</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="105"/>
        <source>The image already exists!</source>
        <translation type="unfinished">Сурет бұрыннан бар!</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="193"/>
        <source>Delete image</source>
        <translation type="unfinished">Кескінді өшіру</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="194"/>
        <source>Are you sure you want to delete this picture?</source>
        <translation type="unfinished">Бұл суретті шынымен жойғыңыз келе ме?</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <source>InputDialog</source>
        <translation type="obsolete">Енгізу диалогы</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../lib/common-widgets/input-dialog/input-dialog.cpp" line="102"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../lib/common-widgets/input-dialog/input-dialog.cpp" line="111"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
</context>
<context>
    <name>InputPage</name>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="14"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="164"/>
        <source>InputPage</source>
        <translation type="unfinished">NPutage</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="40"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="165"/>
        <source>Input devices</source>
        <translation type="unfinished">Кіріс құрылғысы</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="66"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="167"/>
        <source>ComboBoxInputDevices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="83"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="169"/>
        <source>Input volume</source>
        <translation type="unfinished">Кіріс мөлшері</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="134"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="172"/>
        <source>SliderVolumeSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.ui" line="154"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_input-page.h" line="174"/>
        <source>Feedback volume</source>
        <translation type="unfinished">Кері байланыс</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/input-page.cpp" line="278"/>
        <source>No input device detected</source>
        <translation type="unfinished">Кіріс құрылғысы табылмады</translation>
    </message>
</context>
<context>
    <name>Ipv4Widget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="180"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="180"/>
        <source>Ipv4Widget</source>
        <translation type="unfinished">IPV 4 виджеті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="37"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="181"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="181"/>
        <source>IPV4 Method</source>
        <translation type="unfinished">IPV4 әдісі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="56"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="183"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="183"/>
        <source>ComboBoxIpv4Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="88"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="185"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="185"/>
        <source>IP Address</source>
        <translation type="unfinished">IP адресі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="107"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="187"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="187"/>
        <source>EditIpv4Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="121"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="189"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="189"/>
        <source>Net Mask</source>
        <translation type="unfinished">Нетмаска</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="140"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="191"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="191"/>
        <source>EditIpv4Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="154"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="193"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="193"/>
        <source>Gateway</source>
        <translation type="unfinished">Шлюз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="173"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="195"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="195"/>
        <source>EditIpv4Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="190"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="197"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="197"/>
        <source>DNS 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="223"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="201"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="201"/>
        <source>DNS 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferred DNS</source>
        <translation type="obsolete">Қалаған DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="209"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="199"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="199"/>
        <source>EditIpv4PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alternate DNS</source>
        <translation type="obsolete">Күту режиміндегі DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="242"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv4-widget.h" line="203"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv4-widget.h" line="203"/>
        <source>EditIpv4AlternateDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="37"/>
        <source>Auto</source>
        <translation type="unfinished">Автоматты</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="38"/>
        <source>Manual</source>
        <translation type="unfinished">Қолмен</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="40"/>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="41"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="227"/>
        <source>Ipv4 address can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="237"/>
        <source>Ipv4 Address invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="247"/>
        <source>NetMask can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="257"/>
        <source>Netmask invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="269"/>
        <source>Ipv4 Gateway invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="282"/>
        <source>Ipv4 Preferred DNS invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="295"/>
        <source>Ipv4 Alternate DNS invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv6Widget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="182"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="182"/>
        <source>Ipv6Widget</source>
        <translation type="unfinished">IPV 6 виджеті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="40"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="183"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="183"/>
        <source>IPV6 Method</source>
        <translation type="unfinished">IPV6 әдісі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="59"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="185"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="185"/>
        <source>ComboBoxIpv6Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="91"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="187"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="187"/>
        <source>IP Address</source>
        <translation type="unfinished">IP адресі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="110"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="189"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="189"/>
        <source>EditIpv6Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="124"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="191"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="191"/>
        <source>Prefix</source>
        <translation type="unfinished">Префиксі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="143"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="193"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="193"/>
        <source>SpinBoxIpv6Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="157"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="195"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="195"/>
        <source>Gateway</source>
        <translation type="unfinished">Шлюз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="176"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="197"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="197"/>
        <source>EditIpv6Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="193"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="199"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="199"/>
        <source>Preferred DNS</source>
        <translation type="unfinished">Қалаған DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="212"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="201"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="201"/>
        <source>EditIpv6PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="226"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="203"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="203"/>
        <source>Alternate DNS</source>
        <translation type="unfinished">Күту режиміндегі DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="245"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_ipv6-widget.h" line="205"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_ipv6-widget.h" line="205"/>
        <source>EditIpv6AlternateDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="36"/>
        <source>Auto</source>
        <translation type="unfinished">Автоматты</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="37"/>
        <source>Manual</source>
        <translation type="unfinished">Қолмен</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="38"/>
        <source>Ignored</source>
        <translation type="unfinished">Елемеу</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="40"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="200"/>
        <source>Ipv6 address can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="211"/>
        <source>Ipv6 address invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="224"/>
        <source>Ipv6 Gateway invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="238"/>
        <source>Ipv6 Preferred DNS invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="251"/>
        <source>Ipv6 Alternate DNS invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IrisPage</name>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="65"/>
        <source>iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="66"/>
        <source>Default Iris device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="67"/>
        <source>Iris feature list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="99"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="147"/>
        <source>Start enroll failed,%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="148"/>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="179"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="173"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="174"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/iris-page.cpp" line="178"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KcpInterface</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Ескерту</translation>
    </message>
</context>
<context>
    <name>KeybindingSubItem</name>
    <message>
        <location filename="../../plugins/keybinding/keybinding-subitem.h" line="46"/>
        <source>Keybinding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeycodeTranslator</name>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="140"/>
        <source>None</source>
        <translation type="unfinished">Ешқайсысы</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="144"/>
        <source>disabled</source>
        <translation type="unfinished">Мүгедек</translation>
    </message>
</context>
<context>
    <name>KiranAccountManager</name>
    <message>
        <source>disable</source>
        <translation type="obsolete">Жарамсыз ету</translation>
    </message>
    <message>
        <source>enable</source>
        <translation type="obsolete">&amp; Қосу</translation>
    </message>
    <message>
        <source>Create new user</source>
        <translation type="obsolete">Жаңа пайдаланушыны құру</translation>
    </message>
</context>
<context>
    <name>KiranAvatarEditor</name>
    <message>
        <location filename="../../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="42"/>
        <source>Avatar Editor</source>
        <translation type="unfinished">Аватар редакторы</translation>
    </message>
    <message>
        <location filename="../../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="91"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="112"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
</context>
<context>
    <name>KiranCpanelAppearance</name>
    <message>
        <source>KiranCpanelAppearance</source>
        <translation type="obsolete">KiranCpanelAppearance</translation>
    </message>
    <message>
        <source>Wallpaper Setting</source>
        <translation type="obsolete">Түсқағазды баптау</translation>
    </message>
    <message>
        <source>Theme Setting</source>
        <translation type="obsolete">Нақышты баптауName</translation>
    </message>
    <message>
        <source>Font Setting</source>
        <translation type="obsolete">Қаріп параметрлері</translation>
    </message>
</context>
<context>
    <name>KiranDatePickerWidget</name>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-date-picker-widget.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-date-picker-widget.h" line="103"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>KiranDisplayConfiguration</name>
    <message>
        <source>KiranDisplayConfiguration</source>
        <translation type="obsolete">IranisplayonFiguation</translation>
    </message>
    <message>
        <source>Copy display</source>
        <translation type="obsolete">Дисплейді көшіру</translation>
    </message>
    <message>
        <source>Extended display</source>
        <translation type="obsolete">Кеңейтілген дисплей</translation>
    </message>
    <message>
        <source>Resolution ratio</source>
        <translation type="obsolete">Ажыратымдылығы</translation>
    </message>
    <message>
        <source>Refresh rate</source>
        <translation type="obsolete">Жаңарту жиілігі</translation>
    </message>
    <message>
        <source>Zoom rate</source>
        <translation type="obsolete">Масштабтау жылдамдығы</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="obsolete">Автоматты</translation>
    </message>
    <message>
        <source>100% (recommended)</source>
        <translation type="obsolete">100% (ұсынылады)</translation>
    </message>
    <message>
        <source>200%</source>
        <translation type="obsolete">200%</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Қолдану</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Жабу</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Ашу</translation>
    </message>
    <message>
        <source>Set as main display</source>
        <translation type="obsolete">Негізгі дисплейді орнату</translation>
    </message>
    <message>
        <source> (recommended)</source>
        <translation type="obsolete">(Ұсынылады)</translation>
    </message>
    <message>
        <source>Is the display normal?</source>
        <translation type="obsolete">Бұл қалыпты ма?</translation>
    </message>
    <message>
        <source>Save current configuration(K)</source>
        <translation type="obsolete">Назардағы конфигурацияны сақтау (K)</translation>
    </message>
    <message>
        <source>Restore previous configuration(R)</source>
        <translation type="obsolete">Алдыңғы конфигурацияны қалпына келтіру (R)</translation>
    </message>
    <message>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation type="obsolete">Дисплей% 1 секундтан кейін алдыңғы конфигурацияны қалпына келтіреді</translation>
    </message>
</context>
<context>
    <name>KiranDisplayConfigurationPanel</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Пішіні</translation>
    </message>
    <message>
        <source>Rotate left 90 degrees</source>
        <translation type="obsolete">90 градусқа солға бұру</translation>
    </message>
    <message>
        <source>Rotate right 90 degrees</source>
        <translation type="obsolete">90 градусқа оңға бұрыңыз</translation>
    </message>
    <message>
        <source>Turn left and right</source>
        <translation type="obsolete">Солға оңға бұрылыңыз</translation>
    </message>
    <message>
        <source>upside down</source>
        <translation type="obsolete">Төменге қарай</translation>
    </message>
    <message>
        <source>Identification display</source>
        <translation type="obsolete">Тану дисплейі</translation>
    </message>
</context>
<context>
    <name>KiranGroupManager</name>
    <message>
        <location filename="../../plugins/group/src/kiran-group-manager.cpp" line="141"/>
        <source>Create new group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranModuleWidget</name>
    <message>
        <location filename="../../lib/common-widgets/kiran-module-widget/kiran-module-widget.ui" line="14"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_kiran-module-widget.h" line="115"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Ескерту</translation>
    </message>
</context>
<context>
    <name>KiranSearchLineEdit</name>
    <message>
        <source>search...</source>
        <translation type="vanished">Іздеу...</translation>
    </message>
</context>
<context>
    <name>KiranTimeDateWidget</name>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-timedate-widget.h" line="167"/>
        <source>KiranTimeDateWidget</source>
        <translation type="unfinished">KiranTimeDateWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.ui" line="171"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-timedate-widget.h" line="170"/>
        <source>Automatic synchronizetion</source>
        <translation type="unfinished">&amp; Авто үндестіру</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.cpp" line="120"/>
        <source>Change Time Zone</source>
        <translation type="unfinished">Уақыт белдеуін өзгерту</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.cpp" line="131"/>
        <source>Set Time Manually</source>
        <translation type="unfinished">Уақытты қолмен орнатыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.cpp" line="147"/>
        <source>Time date format setting</source>
        <translation type="unfinished">Уақыт пен күнді пішімдеу</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/kiran-timedate-widget.cpp" line="196"/>
        <source>%1(%2)</source>
        <translation type="unfinished">%1(%2)</translation>
    </message>
</context>
<context>
    <name>KiranTimePickerWidget</name>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-picker-widget.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-time-picker-widget.h" line="135"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>KiranTimeZone</name>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-zone.ui" line="23"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-time-zone.h" line="121"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-zone.cpp" line="95"/>
        <source>Search in all time zones...</source>
        <translation type="unfinished">Барлық уақыт белдеулерін іздеу...</translation>
    </message>
</context>
<context>
    <name>KiranTimeZoneItem</name>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-zone-item.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-time-zone-item.h" line="63"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-zone-item.cpp" line="70"/>
        <source>No search results, please search again...</source>
        <translation type="unfinished">Іздеу нәтижелері жоқ, қайта іздеңіз...</translation>
    </message>
</context>
<context>
    <name>KiranTimeZoneList</name>
    <message>
        <location filename="../../plugins/timedate/widgets/kiran-time-zone-list.ui" line="20"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_kiran-time-zone-list.h" line="71"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>KiranTips</name>
    <message>
        <location filename="../../lib/common-widgets/kiran-tips/kiran-tips.ui" line="29"/>
        <location filename="../../plugins/group/src/widgets/kiran-tips.ui" line="29"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_kiran-tips.h" line="71"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>KylinsecLogo</name>
    <message>
        <location filename="../../plugins/system/kylinsec-logo.cpp" line="36"/>
        <source>Copyright ©</source>
        <translation type="unfinished">Авторлық құқық ©</translation>
    </message>
    <message>
        <location filename="../../plugins/system/kylinsec-logo.cpp" line="36"/>
        <source>KylinSec. All rights reserved.</source>
        <translation type="unfinished">Карин Сайк. Барлық құқықтар қорғалған.</translation>
    </message>
</context>
<context>
    <name>LayoutItem</name>
    <message>
        <location filename="../../plugins/keyboard/utils/layout-item.ui" line="14"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-item.h" line="61"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
</context>
<context>
    <name>LayoutList</name>
    <message>
        <location filename="../../plugins/keyboard/utils/layout-list.ui" line="20"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-list.h" line="152"/>
        <source>LayoutList</source>
        <translation type="unfinished">Орналасу тізімі</translation>
    </message>
</context>
<context>
    <name>LayoutPage</name>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="14"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="226"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="67"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="227"/>
        <source>Select Kayboard Layout</source>
        <translation type="unfinished">Kayboard орналасуын таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="105"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="228"/>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="383"/>
        <source>Edit</source>
        <translation type="unfinished">Өңдеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="206"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="230"/>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="265"/>
        <source>Add Layout</source>
        <translation type="unfinished">Орналасуды қосу</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="297"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="232"/>
        <source>ButtonAddLayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="300"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="234"/>
        <source>Addition</source>
        <translation type="unfinished">&amp; Қосу</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="341"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="236"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.ui" line="344"/>
        <location filename="../../build/plugins/keyboard/kiran-cpanel-keyboard_autogen/include/ui_layout-page.h" line="238"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="112"/>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="287"/>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="326"/>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="359"/>
        <source>Failed</source>
        <translation type="unfinished">&amp; Жетіспеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="112"/>
        <source>You have added this keyboard layout!</source>
        <translation type="unfinished">Сіз бұл пернетақта орналасуын қостыңыз!</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="288"/>
        <source>The %1 keyboard layout does not exist!</source>
        <translation type="unfinished">% 1 пернетақта орналасуы жоқ!</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="327"/>
        <source>The keyboard layout is currently in use and cannot be deleted!</source>
        <translation type="unfinished">Пернетақта орналасуы қазіргі уақытта қолданылуда және оны жою мүмкін емес!</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="339"/>
        <source>Delete Layout</source>
        <translation type="unfinished">Орналасуды жою</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="360"/>
        <source>You do not appear to have added %1 keyboard layout!</source>
        <translation type="unfinished">Сіз% 1 пернетақта орналасуын қоспаған сияқтысыз!</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/pages/layout-page.cpp" line="374"/>
        <source>Finish</source>
        <translation type="unfinished">Аяқталды</translation>
    </message>
</context>
<context>
    <name>LayoutSubItem</name>
    <message>
        <location filename="../../plugins/keyboard/layout-subitem.h" line="46"/>
        <source>Keyboard Layout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicenseAgreement</name>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="32"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="124"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="88"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="126"/>
        <source>BrowserLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="103"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="128"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;! DOCTYPE HTML PUBLIC «-//w3c//DTD HTML 4.0//EN » http://www. w3. org/TR/rec-html 40/strict. dtd &gt; 
&lt; html &gt; &lt; head &gt; &lt; meta name = « qrichtext » мазмұны = « 1 » /&gt; &lt; style type = « text/css» &gt; 
P, li {white- space: pre- wrap;} 
&lt; /style &gt; &lt; /head &gt; &lt; body style = « font- family: &apos;Noto Sans CJKSC&apos;; font- size: 9pt; font- syle: 400; font- style: normal; » &gt; &gt; 
&lt; pstyle = « -qt- paragraph- type: empty; margin- top: 0px; margin- bottom: 0px; margin- botht: 0px; margin- botht: 0px; text- block- indent: 0; text- indent: 0px; &gt; &lt; br /&gt; &lt; /p&gt; &lt; /body &gt; &lt; /html &gt; </translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="150"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="134"/>
        <source>ButtonExportLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="153"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="136"/>
        <source>Export</source>
        <translation type="unfinished">Шығу</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="194"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="138"/>
        <source>ButtonCloseLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.ui" line="197"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_license-agreement.h" line="140"/>
        <source>Close</source>
        <translation type="unfinished">Жабу</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="84"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="86"/>
        <source>PDF(*.pdf)</source>
        <translation type="unfinished">PDF (*. PDF)</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="95"/>
        <source>Export License</source>
        <translation type="unfinished">Экспортқа лицензия</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="96"/>
        <source>Export License failed!</source>
        <translation type="unfinished">Экспорт лицензиясы сәтсіз аяқталды!</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="138"/>
        <source>User End License Agreement</source>
        <translation type="unfinished">Пайдаланушы терминалының лицензиялық келісімі</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="157"/>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="170"/>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="229"/>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="237"/>
        <source>None</source>
        <translation type="unfinished">Ешқайсысы</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/license-agreement.cpp" line="185"/>
        <source>Version License</source>
        <translation type="unfinished">Нұсқа лицензиясы</translation>
    </message>
</context>
<context>
    <name>ManagerTray</name>
    <message>
        <source>Network settings</source>
        <translation type="obsolete">Желі параметрлері</translation>
    </message>
</context>
<context>
    <name>Media Key</name>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="69"/>
        <source>Audio Play</source>
        <translation type="unfinished">Дыбысты ойнату</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="70"/>
        <source>Search</source>
        <translation type="unfinished">Іздеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="71"/>
        <source>WWW</source>
        <translation type="unfinished">Дүниежүзілік ғаламтор</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="72"/>
        <source>Audio Lower Volume</source>
        <translation type="unfinished">Дыбыс деңгейі төмен</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="73"/>
        <source>Audio Raise Volume</source>
        <translation type="unfinished">Дыбыс деңгейін жоғарылату</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="74"/>
        <source>Mic Mute</source>
        <translation type="unfinished">Микрофонды өшіру</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="75"/>
        <source>Audio Stop</source>
        <translation type="unfinished">Дыбысты тоқтату</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="76"/>
        <source>Explorer</source>
        <translation type="unfinished">Зерттеуші</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="77"/>
        <source>Calculator</source>
        <translation type="unfinished">Калькулятор</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="78"/>
        <source>Audio Mute</source>
        <translation type="unfinished">Дыбысты өшіру</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="79"/>
        <source>Audio Pause</source>
        <translation type="unfinished">Дыбысты кідірту</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="80"/>
        <source>Audio Prev</source>
        <translation type="unfinished">Аудио алдыңғы бет</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="81"/>
        <source>Audio Media</source>
        <translation type="unfinished">Аудио медиа</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="82"/>
        <source>Audio Next</source>
        <translation type="unfinished">&amp; Келесі аудио</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="83"/>
        <source>Mail</source>
        <translation type="unfinished">Пошта</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="84"/>
        <source>Tools</source>
        <translation type="unfinished">Құралдар</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/keycode-translator.cpp" line="85"/>
        <source>Eject</source>
        <translation type="unfinished">Орындау</translation>
    </message>
</context>
<context>
    <name>MonthSpinBox</name>
    <message>
        <location filename="../../plugins/timedate/widgets/date-spinbox.h" line="51"/>
        <source>MMMM</source>
        <translation type="unfinished">Хм</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="14"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="242"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="68"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="243"/>
        <source>Select Mouse Hand</source>
        <translation type="unfinished">Тінтуір меңзерін таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="103"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="245"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="126"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="247"/>
        <source>Mouse Motion Acceleration</source>
        <translation type="unfinished">Тінтуірдің үдеуі</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="133"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="249"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="163"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="251"/>
        <source>Slow</source>
        <translation type="unfinished">Баяу</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="183"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="252"/>
        <source>Fast</source>
        <translation type="unfinished">Жылдам</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="216"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="253"/>
        <source>Natural Scroll</source>
        <translation type="unfinished">Табиғи құйын</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="236"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="255"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="269"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="258"/>
        <source>Middle Emulation Enabled</source>
        <translation type="unfinished">Аралық модельдеу қосылған</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="289"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="260"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="335"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="263"/>
        <source>Test mouse wheel direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.ui" line="372"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_mouse-page.h" line="264"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.cpp" line="80"/>
        <source>Right Hand Mode</source>
        <translation type="unfinished">Оң жақтағы режим</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/mouse-page.cpp" line="80"/>
        <source>Left Hand Mode</source>
        <translation type="unfinished">Сол жақтағы режим</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../../plugins/mouse/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetworkDetails</name>
    <message>
        <source>NetworkDetails</source>
        <translation type="obsolete">Желі мәліметтері</translation>
    </message>
    <message>
        <source>Network Details</source>
        <translation type="obsolete">Желі мәліметтері</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">Мәтін тегтері</translation>
    </message>
</context>
<context>
    <name>NetworkPlugin</name>
    <message>
        <source>Wired Connection %1</source>
        <translation type="obsolete">% 1 сымды қосылым</translation>
    </message>
    <message>
        <source>Wireless Connection %1</source>
        <translation type="obsolete">% 1 сымсыз байланыс</translation>
    </message>
</context>
<context>
    <name>NetworkSubItem</name>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="111"/>
        <source>Wired Network %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="115"/>
        <source>Wired Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="124"/>
        <source>Wireless Network %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="129"/>
        <source>Wireless Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="136"/>
        <source>VPN</source>
        <translation type="unfinished">Виртуалды жеке желі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/network-subitem.cpp" line="137"/>
        <source>Network Details</source>
        <translation type="unfinished">Желі мәліметтері</translation>
    </message>
</context>
<context>
    <name>NetworkTray</name>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="169"/>
        <source>Network settings</source>
        <translation type="unfinished">Желі параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="233"/>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="505"/>
        <source>Network unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="465"/>
        <source>Wired network card: %1 available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="470"/>
        <source>Wireless network card: %1 available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="511"/>
        <source>Wired network card: %1 unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/network-tray.cpp" line="516"/>
        <source>Wireless network card: %1 unavailable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputPage</name>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="14"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="199"/>
        <source>OutputPage</source>
        <translation type="unfinished">Шығу парағы</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="49"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="200"/>
        <source>Output devices</source>
        <translation type="unfinished">Шығу құрылғысы</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="78"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="202"/>
        <source>ComboBoxOutputDevices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="101"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="204"/>
        <source>Output volume</source>
        <translation type="unfinished">Шығу</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="158"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="207"/>
        <source>SlilderVolumeSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="184"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="209"/>
        <source>Left/right balance</source>
        <translation type="unfinished">Сол/оң баланс</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="222"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="211"/>
        <source>SliderVolumeBalance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="236"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="213"/>
        <source>Left</source>
        <translation type="unfinished">Сол жақта</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.ui" line="262"/>
        <location filename="../../build/plugins/audio/kiran-cpanel-audio_autogen/include/ui_output-page.h" line="214"/>
        <source>Right</source>
        <translation type="unfinished">Иә</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/plugin/output-page.cpp" line="215"/>
        <source>No output device detected</source>
        <translation type="unfinished">Шығу құрылғысы табылмады</translation>
    </message>
</context>
<context>
    <name>PanelWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Пішіні</translation>
    </message>
</context>
<context>
    <name>PanelWindow</name>
    <message>
        <location filename="../../src/panel-window.cpp" line="46"/>
        <source>Control Panel</source>
        <translation type="unfinished">Басқару тақтасы</translation>
    </message>
</context>
<context>
    <name>PasswordExpirationPolicyPage</name>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="14"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="248"/>
        <source>PasswordExpirationPolicyPage</source>
        <translation type="unfinished">PasswordExpirationPolicyPage</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="43"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="249"/>
        <source>User expires</source>
        <translation type="unfinished">Пайдаланушының мерзімі бітті</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="67"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="251"/>
        <source>SpinBoxUserExpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="70"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="253"/>
        <source>yyyy-MM-dd</source>
        <translation type="unfinished">Yyyyy-MM-dd</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="87"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="254"/>
        <source>Last password change</source>
        <translation type="unfinished">Соңғы пароль өзгертілді</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="111"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="256"/>
        <source>LabelLastPasswdChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="114"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="258"/>
        <source>1990-01-01</source>
        <translation type="unfinished">1990-01-01</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="131"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="259"/>
        <source>Maximum vaild days of password</source>
        <translation type="unfinished">Парольдің максималды жарамды күндері</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="155"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="261"/>
        <source>SpinBoxMaximumValidDays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="172"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="263"/>
        <source>Prompt time before password expiration</source>
        <translation type="unfinished">Парольдің жарамдылық мерзімі аяқталғанға дейін сұрау уақыты</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="196"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="265"/>
        <source>SpinBoxPromptBeforeExpiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="213"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="267"/>
        <source>how many days after password expires will become inactive</source>
        <translation type="unfinished">Құпия сөздің мерзімі аяқталғаннан кейін қанша күн белсенді болмайды</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="237"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="269"/>
        <source>SpinBoxPasswdInactiveTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="298"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="272"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="301"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="274"/>
        <source>save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="342"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="276"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="345"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_password-expiration-policy-page.h" line="278"/>
        <source>return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="73"/>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="80"/>
        <location filename="../../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="88"/>
        <source>day</source>
        <translation type="unfinished">Күн</translation>
    </message>
</context>
<context>
    <name>PluginConnectionList</name>
    <message>
        <location filename="../../plugins/network/src/plugin/plugin-connection-list.cpp" line="178"/>
        <source>Other WiFi networks</source>
        <translation type="unfinished">Басқа WiFi желілері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/plugin-connection-list.cpp" line="285"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/plugin-connection-list.cpp" line="286"/>
        <source>Please input a network name</source>
        <translation type="unfinished">Желі атауын енгізіңіз</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/popup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/popup.ui" line="26"/>
        <source>Ok</source>
        <translation type="unfinished">Жарайды</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/popup.ui" line="39"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/popup.cpp" line="23"/>
        <source>cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
</context>
<context>
    <name>PowerPlugin</name>
    <message>
        <location filename="../../plugins/power/power-plugin.cpp" line="55"/>
        <source>General Settings</source>
        <translation type="unfinished">Жалпы баптаулары</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-plugin.cpp" line="64"/>
        <source>Power Settings</source>
        <translation type="unfinished">Қуат параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-plugin.cpp" line="79"/>
        <source>Battery Settings</source>
        <translation type="unfinished">Батарея параметрлері</translation>
    </message>
</context>
<context>
    <name>PowerProfilesWrapper</name>
    <message>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="97"/>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="113"/>
        <source>power-saver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="98"/>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="114"/>
        <source>balanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="99"/>
        <location filename="../../plugins/power/dbus/power-profiles-wrapper.cpp" line="115"/>
        <source>performance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerSettingsPage</name>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="14"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="112"/>
        <source>PowerSettingsPage</source>
        <translation type="unfinished">Қуат параметрлері беті</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="43"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="113"/>
        <source>After idle for more than the following time, the computer will execute</source>
        <translation type="unfinished">Компьютер келесі уақыттан артық жұмыс істемей тұрғаннан кейін жұмыс істейді</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="60"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="115"/>
        <source>ComboIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="67"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="118"/>
        <source>ComboIdleAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="84"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="120"/>
        <source>The monitor will turn off when it is idle</source>
        <translation type="unfinished">Монитор жұмыс істемей тұрғанда өшіріледі</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.ui" line="101"/>
        <location filename="../../build/plugins/power/kiran-cpanel-power_autogen/include/ui_power-settings-page.h" line="122"/>
        <source>ComboMonitorTrunOffIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.cpp" line="62"/>
        <source>Suspend</source>
        <translation type="unfinished">&amp; Кідірту</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.cpp" line="63"/>
        <source>Shutdown</source>
        <translation type="unfinished">Жабу</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.cpp" line="64"/>
        <source>Hibernate</source>
        <translation type="unfinished">Ұйқы</translation>
    </message>
    <message>
        <location filename="../../plugins/power/pages/power-settings-page.cpp" line="65"/>
        <source>Do nothing</source>
        <translation type="unfinished">Ештеңе жасамаңыз</translation>
    </message>
</context>
<context>
    <name>PowerSubItem</name>
    <message>
        <source>Power Settings</source>
        <translation type="obsolete">Қуат параметрлері</translation>
    </message>
</context>
<context>
    <name>PrefsPage</name>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="64"/>
        <source>Authentication type Enabled status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="75"/>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="178"/>
        <source>fingerprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="76"/>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="178"/>
        <source>fingervein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="77"/>
        <source>ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="78"/>
        <source>iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="79"/>
        <source>face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="115"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="130"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="138"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="139"/>
        <source>unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="140"/>
        <source>empowerment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/prefs-page.cpp" line="179"/>
        <source>Apply the %1 authentication to the following applications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.cpp" line="175"/>
        <source>Failed</source>
        <translation type="unfinished">&amp; Жетіспеу</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/font/fonts.cpp" line="176"/>
        <source>Set font failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set font  failed!</source>
        <translation type="obsolete">Қаріпті орнату сәтсіз аяқталды!</translation>
    </message>
    <message>
        <source>Get icon themes failed!</source>
        <translation type="obsolete">Белгіше нақышын алу сәтсіз аяқталды!</translation>
    </message>
    <message>
        <source>Get cursor themes failed!</source>
        <translation type="obsolete">Меңзер нақышын алу сәтсіз аяқталды!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Ескерту</translation>
    </message>
    <message>
        <source>There is no theme to set!</source>
        <translation type="obsolete">Ешқандай тақырыпты орнатуға болмайды!</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-config.cpp" line="466"/>
        <location filename="../../plugins/display/src/display-config.h" line="134"/>
        <location filename="../../plugins/display/src/display-page.cpp" line="400"/>
        <location filename="../../plugins/display/src/display-page.cpp" line="418"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-config.cpp" line="469"/>
        <location filename="../../plugins/display/src/display-config.h" line="137"/>
        <location filename="../../plugins/display/src/display-page.cpp" line="403"/>
        <location filename="../../plugins/display/src/display-page.cpp" line="421"/>
        <source>OK(K)</source>
        <translation type="unfinished">Жарайды (K)</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="407"/>
        <source>Failed to apply display settings!%1</source>
        <translation type="unfinished">Бағдарлама дисплейін орнату сәтсіз аяқталды! % 1</translation>
    </message>
    <message>
        <location filename="../../plugins/display/src/display-page.cpp" line="425"/>
        <source>Fallback display setting failed! %1</source>
        <translation type="unfinished">Backback дисплей параметрлері сәтсіз аяқталды! % 1</translation>
    </message>
    <message>
        <location filename="../../plugins/keyboard/utils/layout-list.cpp" line="142"/>
        <source>No search results, please search again...</source>
        <translation type="unfinished">Іздеу нәтижелері жоқ, қайта іздеңіз...</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-utils.cpp" line="32"/>
        <source>%1Day</source>
        <translation type="unfinished">Amount in units (real)</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-utils.cpp" line="36"/>
        <source>%1Hour</source>
        <translation type="unfinished">Amount in units (real)</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-utils.cpp" line="40"/>
        <source>%1Minute</source>
        <translation type="unfinished">Amount in units (real)</translation>
    </message>
    <message>
        <location filename="../../plugins/power/power-utils.cpp" line="44"/>
        <source>never</source>
        <translation type="unfinished">Ешқашан</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../../src/search-edit/search-edit.cpp" line="45"/>
        <source>Enter keywords to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/search-edit/search-edit.cpp" line="84"/>
        <source>Info</source>
        <translation type="unfinished">Ақпарат</translation>
    </message>
    <message>
        <location filename="../../src/search-edit/search-edit.cpp" line="84"/>
        <source>Failed to find related items, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectAvatarPage</name>
    <message>
        <location filename="../../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="148"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="162"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="179"/>
        <source>select picture</source>
        <translation type="unfinished">Суретті таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="181"/>
        <source>image files(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</source>
        <translation type="unfinished">Сурет файлы (*. bmp *. jpg *. png *. tif *. gif *. tga *. exif *. fpx *. svg *. pcd *. dxf *. ufo *. eps *. ai *. raw *. WMF *. webp)</translation>
    </message>
</context>
<context>
    <name>SettingBriefWidget</name>
    <message>
        <location filename="../../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="26"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_setting-brief-widget.h" line="68"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="47"/>
        <location filename="../../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="67"/>
        <location filename="../../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="74"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_setting-brief-widget.h" line="69"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_setting-brief-widget.h" line="70"/>
        <location filename="../../build/lib/common-widgets/common-widgets_autogen/include/ui_setting-brief-widget.h" line="71"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="14"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="462"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="78"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="464"/>
        <source>EditSearch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="178"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="467"/>
        <source>Custom</source>
        <translation type="unfinished">Әдеті</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="210"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="468"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="163"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="550"/>
        <source>Edit</source>
        <translation type="unfinished">Өңдеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="343"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="470"/>
        <source>ButtonAddShortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="346"/>
        <location filename="../../plugins/keybinding/shortcut.ui" line="551"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="472"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="489"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="106"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="118"/>
        <source>Add</source>
        <translation type="unfinished">&amp; Плюс</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="381"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="474"/>
        <source>ButtonReset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="384"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="476"/>
        <source>Reset</source>
        <translation type="unfinished">Іс</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="430"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="477"/>
        <source>Custom Shortcut Name</source>
        <translation type="unfinished">Stom Dectcut</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="449"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="479"/>
        <source>EditCustomShortcutName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="463"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="481"/>
        <source>Custom Shortcut application</source>
        <translation type="unfinished">Өзгертпелі тіркесім қолданбасы</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="482"/>
        <location filename="../../plugins/keybinding/shortcut.ui" line="700"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="483"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="500"/>
        <source>EditShortcutApp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="496"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="485"/>
        <source>Custom Shortcut Key</source>
        <translation type="unfinished">Өзгертпелі тіркесімдер</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="548"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="487"/>
        <source>ButtonAdd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="586"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="491"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="589"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="493"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="635"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="494"/>
        <source>Shortcut Name</source>
        <translation type="unfinished">Төте жол атауы</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="654"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="496"/>
        <source>EditShortcutName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="681"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="498"/>
        <source>Shortcut application</source>
        <translation type="unfinished">Жылдам қолданба</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="715"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="502"/>
        <source>Shortcut key</source>
        <translation type="unfinished">Пернелер тіркесімі</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="767"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="504"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="770"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="506"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="805"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="508"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.ui" line="808"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut.h" line="510"/>
        <source>return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="87"/>
        <source>Please enter a search keyword...</source>
        <translation type="unfinished">Іздеу кілтін келтіріңіз...</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="99"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="127"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="134"/>
        <source>Please press the new shortcut key</source>
        <translation type="unfinished">Жаңа тіркесімді басыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="161"/>
        <source>Finished</source>
        <translation type="unfinished">Аяқталды</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="655"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="705"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="726"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="761"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="782"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="805"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="825"/>
        <source>Failed</source>
        <translation type="unfinished">&amp; Жетіспеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="301"/>
        <source>Get shortcut failed,error:</source>
        <translation type="unfinished">Төте жолды алу қатесі:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="215"/>
        <source>failed to load shortcut key data!</source>
        <translation type="unfinished">Пернелер тіркесімі деректерін жүктеу мүмкін емес!</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="257"/>
        <source>List shortcut failed,error:%1</source>
        <translation type="unfinished">Тізімнің тіркесімі жаңылысы:% 1</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="300"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="379"/>
        <source>Open File</source>
        <translation type="unfinished">Файлды ашу</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="603"/>
        <source>System</source>
        <translation type="unfinished">Жүйе</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="605"/>
        <source>Sound</source>
        <translation type="unfinished">Дыбыс</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="656"/>
        <source>Delete shortcut failed,error:</source>
        <translation type="unfinished">Төте жолды өшіру қатесі:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="669"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="744"/>
        <source>Warning</source>
        <translation type="unfinished">Ескерту</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="670"/>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="745"/>
        <source>Please complete the shortcut information!</source>
        <translation type="unfinished">Төте жол ақпаратын толтырыңыз!</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="679"/>
        <source>Set shortcut</source>
        <translation type="unfinished">Төте жолдарды орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="680"/>
        <source>Are you sure you want to disable this shortcut?</source>
        <translation type="unfinished">Бұл тіркесімді өшіргіңіз келе ме?</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="706"/>
        <source>Modify system shortcut failed,error:</source>
        <translation type="unfinished">Жүйе тіркесімін өзгерту сәтсіз аяқталды, қате:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="727"/>
        <source>Modify custom shortcut failed,error:</source>
        <translation type="unfinished">Өзгертпелі тіркесімді өзгерту қатесі:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="762"/>
        <source>Add custom shortcut failed,error:</source>
        <translation type="unfinished">Өзгертпелі тіркесімді қосу қатесі:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="783"/>
        <source>Reset shortcut failed,error:</source>
        <translation type="unfinished">Төте жолды қалпына келтіру қатесі:</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="806"/>
        <source>Cannot use shortcut &quot;%1&quot;, Because you cannot enter with this key.Please try again using Ctrl, Alt, or Shift at the same time.</source>
        <translation type="unfinished">«% 1» тіркесімін пайдалану мүмкін емес, өйткені бұл кілтпен кіру мүмкін емес. Ctrl, Alt немесе Shift екеуін де қайталап көріңіз.</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/shortcut.cpp" line="826"/>
        <source>Shortcut keys %1 are already used in %2,Please try again!</source>
        <translation type="unfinished">% 1 тіркесімі% 2 қолданылуда. Әрекетті қайталаңыз!</translation>
    </message>
</context>
<context>
    <name>ShortcutItem</name>
    <message>
        <location filename="../../plugins/keybinding/utils/shortcut-item.ui" line="32"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut-item.h" line="76"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/shortcut-item.ui" line="53"/>
        <location filename="../../plugins/keybinding/utils/shortcut-item.ui" line="73"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut-item.h" line="77"/>
        <location filename="../../build/plugins/keybinding/kiran-cpanel-keybinding_autogen/include/ui_shortcut-item.h" line="78"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
</context>
<context>
    <name>StatusNotification</name>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="70"/>
        <source>Connection activated</source>
        <translation type="unfinished">Қосылым іске қосылды</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="71"/>
        <source>You are now connected to the network &quot;%1&quot;</source>
        <translation type="unfinished">Сіз қазір &apos;% 1&apos; желісіне қосылдыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="80"/>
        <source>Connection deactivated</source>
        <translation type="unfinished">Қосылым өшірілді</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="81"/>
        <source>You have now disconnected the network &quot;%1&quot;</source>
        <translation type="unfinished">Сіз қазір &apos;% 1&apos; желісінен ажыратылдыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="89"/>
        <source>Connection deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="90"/>
        <source>The connection has been deleted &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="26"/>
        <location filename="../../plugins/network/src/status-notification.cpp" line="34"/>
        <location filename="../../plugins/network/src/status-notification.cpp" line="45"/>
        <location filename="../../plugins/network/src/status-notification.cpp" line="54"/>
        <location filename="../../plugins/network/src/status-notification.cpp" line="62"/>
        <source>Connection Failed</source>
        <translation type="unfinished">Қосылым сәтсіз аяқталды</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="27"/>
        <source>the network not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="35"/>
        <source>The hidden network &quot;%1&quot; to be connected has been detected and exists in the network list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/status-notification.cpp" line="46"/>
        <location filename="../../plugins/network/src/status-notification.cpp" line="55"/>
        <source>Failed to connect to the network &quot;%1&quot;</source>
        <translation type="unfinished">&quot;% 1&quot; желісіне қосылу мүмкін емес</translation>
    </message>
</context>
<context>
    <name>SubItem1</name>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/subitem-1.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Диалог</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/subitem-1.ui" line="20"/>
        <source>弹窗</source>
        <translation type="unfinished">&amp; Қалқымалы терезе</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/subitem-1.ui" line="27"/>
        <source>SubItem1</source>
        <translation type="unfinished">SubItem1</translation>
    </message>
</context>
<context>
    <name>SubItem2</name>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/subitem-2.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Диалог</translation>
    </message>
    <message>
        <location filename="../../example/kiran-cpanel-demo/src/subitem/subitem-2.ui" line="20"/>
        <source>SubItem2</source>
        <translation type="unfinished">SubItem2Stencils</translation>
    </message>
</context>
<context>
    <name>SystemInfoSubItem</name>
    <message>
        <location filename="../../plugins/system/system-subitem.h" line="33"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemInformation</name>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="14"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="342"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="152"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="346"/>
        <source>Host Name:</source>
        <translation type="unfinished">Хосттың атауы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="172"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="348"/>
        <source>LabelHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="175"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="272"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="350"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="416"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="479"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="350"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="359"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="364"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="369"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="371"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="206"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="352"/>
        <source>ButtonChangeHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="249"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="355"/>
        <source>System Version:</source>
        <translation type="unfinished">Жүйелік нұсқасы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="269"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="357"/>
        <source>LabelSystemVersion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="327"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="360"/>
        <source>Kernel Version:</source>
        <translation type="unfinished">Ядро нұсқасы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="347"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="362"/>
        <source>LabelKernelVersion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="393"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="365"/>
        <source>System Architecture:</source>
        <translation type="unfinished">Жүйе архитектурасы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="413"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="367"/>
        <source>LabelSystemArch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="459"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="370"/>
        <source>Activation status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="544"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="373"/>
        <source>EULA:</source>
        <translation type="unfinished">EULA:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="576"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="375"/>
        <source>ButtonShowEULA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="619"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="378"/>
        <source>Version License:</source>
        <translation type="unfinished">Нұсқа лицензиясы:</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="651"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="380"/>
        <source>ButtonShowVersionLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="209"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="354"/>
        <source>Change</source>
        <translation type="unfinished">Өзгерту</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="504"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="579"/>
        <location filename="../../plugins/system/pages/system-information/system-information.ui" line="654"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="372"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="377"/>
        <location filename="../../build/plugins/system/kiran-cpanel-system_autogen/include/ui_system-information.h" line="382"/>
        <source>Show</source>
        <translation type="unfinished">&amp; Көрсету</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="98"/>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="99"/>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="100"/>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="101"/>
        <source>Unknow</source>
        <translation type="unfinished">Білмеймін</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="236"/>
        <source>UnActivated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="245"/>
        <source>Activation code has expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="250"/>
        <source>Permanently activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="255"/>
        <source>Activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="298"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/system/pages/system-information/system-information.cpp" line="298"/>
        <source>Failed to open the license activator</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextInputDialog</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="40"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="43"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="44"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
</context>
<context>
    <name>ThemePage</name>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.ui" line="14"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_theme-page.h" line="167"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.ui" line="81"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_theme-page.h" line="168"/>
        <source>Dark and Light Theme</source>
        <translation type="unfinished">Қараңғы және жарық тақырыбы</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.ui" line="109"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_theme-page.h" line="169"/>
        <source>Themes Settings</source>
        <translation type="unfinished">Нақышты баптауName</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.ui" line="149"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_theme-page.h" line="170"/>
        <source>Open Window Effects</source>
        <translation type="unfinished">Терезені ашу эффектісі</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="88"/>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="122"/>
        <source>Unknown</source>
        <translation type="unfinished">Белгісіз</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="82"/>
        <source>Choose icon Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="116"/>
        <source>Choose cursor Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="155"/>
        <source>Light Theme</source>
        <translation type="unfinished">Жарық нақышы</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="156"/>
        <source>Auto</source>
        <translation type="unfinished">Автоматты</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/theme/theme-page.cpp" line="157"/>
        <source>Dark Theme</source>
        <translation type="unfinished">Қараңғы нақыштар</translation>
    </message>
</context>
<context>
    <name>ThemeWidget</name>
    <message>
        <source>Dark Theme</source>
        <translation type="obsolete">Қараңғы нақыштар</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation type="obsolete">Жарық нақышы</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">Автоматты</translation>
    </message>
</context>
<context>
    <name>Themes</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Пішіні</translation>
    </message>
    <message>
        <source>Dark and Light Theme</source>
        <translation type="obsolete">Қараңғы және жарық тақырыбы</translation>
    </message>
    <message>
        <source>Themes Settings</source>
        <translation type="obsolete">Нақышты баптауName</translation>
    </message>
    <message>
        <source>Open Window Effects</source>
        <translation type="obsolete">Терезені ашу эффектісі</translation>
    </message>
    <message>
        <source>Choose icon themes</source>
        <translation type="obsolete">Таңбаша нақышын таңдау</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Белгісіз</translation>
    </message>
    <message>
        <source>Choose cursor themes</source>
        <translation type="obsolete">Меңзердің нақышын таңдау</translation>
    </message>
</context>
<context>
    <name>ThreadObject</name>
    <message>
        <location filename="../../plugins/keybinding/utils/thread-object.cpp" line="137"/>
        <source>Failed</source>
        <translation type="unfinished">&amp; Жетіспеу</translation>
    </message>
    <message>
        <location filename="../../plugins/keybinding/utils/thread-object.cpp" line="138"/>
        <source>List shortcut failed,error:</source>
        <translation type="unfinished">Тізімнің тіркесімі қатесі:</translation>
    </message>
</context>
<context>
    <name>TimeDateSubItem</name>
    <message>
        <location filename="../../plugins/timedate/timedate-subitem.cpp" line="44"/>
        <source>Time Date Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/timedate-subitem.cpp" line="76"/>
        <source>Chnage time Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/timedate-subitem.cpp" line="77"/>
        <source>Set time Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/timedate-subitem.cpp" line="78"/>
        <source>Time date format setting</source>
        <translation type="unfinished">Уақыт пен күнді пішімдеу</translation>
    </message>
</context>
<context>
    <name>TimezoneSettings</name>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="14"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="125"/>
        <source>TimezoneSettings</source>
        <translation type="unfinished">Уақыт белдеуінің параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="52"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="126"/>
        <source>Select Time Zone</source>
        <translation type="unfinished">Уақыт белдеуін таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="116"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="128"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="119"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="130"/>
        <source>save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="160"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="132"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="163"/>
        <location filename="../../build/plugins/timedate/kiran-cpanel-timedate_autogen/include/ui_time-zone-settings.h" line="134"/>
        <source>reset</source>
        <translation type="unfinished">Қалпына келтіру</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="14"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="321"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="86"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="322"/>
        <source>TouchPad Enabled</source>
        <translation type="unfinished">Сенсорлық панель қосылған</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="106"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="324"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="137"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="327"/>
        <source>Select TouchPad Hand</source>
        <translation type="unfinished">Сенсорлық тақтаның тұтқасын таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="156"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="329"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="189"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="331"/>
        <source>TouchPad Motion Acceleration</source>
        <translation type="unfinished">Сенсорлық тақтаның қозғалысының үдеуі</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="211"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="333"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="235"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="335"/>
        <source>Slow</source>
        <translation type="unfinished">Баяу</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="255"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="336"/>
        <source>Fast</source>
        <translation type="unfinished">Жылдам</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="285"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="337"/>
        <source>Select Click Method</source>
        <translation type="unfinished">Басу әдісін таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="304"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="339"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="332"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="341"/>
        <source>Select Scroll Method</source>
        <translation type="unfinished">Айналдыру әдісін таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="351"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="343"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="379"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="345"/>
        <source>Natural Scroll</source>
        <translation type="unfinished">Табиғи құйын</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="399"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="347"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="430"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="350"/>
        <source>Enabled while Typing</source>
        <translation type="unfinished">Теру кезінде &amp; рұқсат етілсін</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="450"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="352"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="481"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="355"/>
        <source>Tap to Click</source>
        <translation type="unfinished">Басу үшін басыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.ui" line="501"/>
        <location filename="../../build/plugins/mouse/kiran-cpanel-mouse_autogen/include/ui_touchpad-page.h" line="357"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation type="unfinished">Оң жақтағы режим</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation type="unfinished">Сол жақтағы режим</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation type="unfinished">Басып басыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation type="unfinished">&amp; Түрту</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation type="unfinished">Екі саусақты айналдыру</translation>
    </message>
    <message>
        <location filename="../../plugins/mouse/pages/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation type="unfinished">Жиекті айналдыру</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../../plugins/mouse/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayConnectionList</name>
    <message>
        <location filename="../../plugins/network/src/tray/tray-connection-list.cpp" line="186"/>
        <source>Other WiFi networks</source>
        <translation type="unfinished">Басқа WiFi желілері</translation>
    </message>
</context>
<context>
    <name>TrayItemWidget</name>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="32"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="254"/>
        <source>TrayItemWidget</source>
        <translation type="unfinished">TrayItemWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="92"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="255"/>
        <source>Icon</source>
        <translation type="unfinished">&amp; Таңбашасы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="99"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="256"/>
        <source>Name</source>
        <translation type="unfinished">Аты</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="187"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="257"/>
        <source>Status</source>
        <translation type="unfinished">Күй- жайы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="263"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="258"/>
        <source>Ignore</source>
        <translation type="unfinished">Елемеу</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="288"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="259"/>
        <source>Disconnect</source>
        <translation type="unfinished">Ажырату</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="355"/>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="477"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="260"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="262"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="380"/>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.ui" line="502"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="261"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-itemwidget.h" line="263"/>
        <source>Connect</source>
        <translation type="unfinished">Қосылу</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.cpp" line="147"/>
        <source>Connected</source>
        <translation type="unfinished">Қосылған</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.cpp" line="158"/>
        <source>Unconnected</source>
        <translation type="unfinished">Байланыстырылмаған</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.cpp" line="192"/>
        <source>Please input password</source>
        <translation type="unfinished">Құпия сөзді енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-itemwidget.cpp" line="201"/>
        <source>Please input a network name</source>
        <translation type="unfinished">Желі атауын енгізіңіз</translation>
    </message>
</context>
<context>
    <name>TrayPage</name>
    <message>
        <location filename="../../plugins/network/src/tray/tray-page.ui" line="32"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-page.h" line="98"/>
        <source>TrayPage</source>
        <translation type="unfinished">Науа парағы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-page.ui" line="98"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_tray-page.h" line="99"/>
        <source>TextLabel</source>
        <translation type="unfinished">Мәтін тегтері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-page.cpp" line="108"/>
        <source>Select wired network card</source>
        <translation type="unfinished">Сымды желі картасын таңдаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/tray/tray-page.cpp" line="113"/>
        <source>Select wireless network card</source>
        <translation type="unfinished">Сымсыз желі картасын таңдаңыз</translation>
    </message>
</context>
<context>
    <name>UKeyPage</name>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="52"/>
        <source>Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="53"/>
        <source>Default Ukey device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="54"/>
        <source>List of devices bound to the Ukey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="74"/>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="88"/>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="126"/>
        <source>error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="89"/>
        <source>No UKey device detected, pelease insert the UKey device and perform operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="95"/>
        <source>UKey Enroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/authentication/pages/ukey-page.cpp" line="96"/>
        <source>Please enter the ukey pin code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserInfoPage</name>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="14"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="450"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="138"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="451"/>
        <source>Account</source>
        <translation type="unfinished">Тіркелгі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="167"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="452"/>
        <source>Change password</source>
        <translation type="unfinished">Парольді өзгерту</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="193"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="453"/>
        <source>User id</source>
        <translation type="unfinished">Пайдаланушы идентификаторы</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="251"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="454"/>
        <source>User type</source>
        <translation type="unfinished">Пайдаланушы түрі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="312"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="455"/>
        <source>User status</source>
        <translation type="unfinished">Пайдаланушының күйі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="367"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="456"/>
        <source>auth manager</source>
        <translation type="unfinished">Аутентификация менеджері</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="396"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="457"/>
        <source>Password expiration policy</source>
        <translation type="unfinished">Құпия сөздің жарамдылық мерзімі</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="461"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="458"/>
        <source>Confirm</source>
        <translation type="unfinished">Растау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="502"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="459"/>
        <source>Delete</source>
        <translation type="unfinished">Жою</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="577"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="460"/>
        <source>Current password</source>
        <translation type="unfinished">Ағымдағы пароль</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="623"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="462"/>
        <source>EditCurrentPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="641"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="464"/>
        <source>New password</source>
        <translation type="unfinished">Жаңа пароль</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="676"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="466"/>
        <source>EditNewPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="693"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="468"/>
        <source>Enter the new password again</source>
        <translation type="unfinished">Жаңа парольді қайта енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="728"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="470"/>
        <source>EditNewPasswdAgain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="789"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="473"/>
        <source>EditPasswdSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="792"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="475"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="833"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="477"/>
        <source>EditPasswdCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.ui" line="836"/>
        <location filename="../../build/plugins/account/kiran-cpanel-account_autogen/include/ui_user-info-page.h" line="479"/>
        <source>Cancel</source>
        <translation type="unfinished">Бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="143"/>
        <source>standard</source>
        <translation type="unfinished">Стандартты</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="144"/>
        <source>administrator</source>
        <translation type="unfinished">Администратор</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="222"/>
        <source>Please enter the new user password</source>
        <translation type="unfinished">Жаңа пайдаланушы паролін енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="230"/>
        <source>Please enter the password again</source>
        <translation type="unfinished">Құпия сөзді қайта енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="236"/>
        <source>The password you enter must be the same as the former one</source>
        <translation type="unfinished">Сіз енгізген пароль алдыңғы парольмен бірдей болуы керек</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="248"/>
        <source>Please enter the current user password</source>
        <translation type="unfinished">Ағымдағы пайдаланушының паролін енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="254"/>
        <source>The current password is incorrect</source>
        <translation type="unfinished">Назардағы пароль дұрыс емес</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="261"/>
        <source>The new password cannot be the same as the current password</source>
        <translation type="unfinished">Жаңа пароль ағымдағы парольмен бірдей болмауы керек</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="267"/>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="275"/>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="313"/>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="332"/>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="366"/>
        <source>Error</source>
        <translation type="unfinished">Қате</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="267"/>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="275"/>
        <source>Password encryption failed</source>
        <translation type="unfinished">Парольді шифрлау жаңылысы</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="318"/>
        <source>user information updated successfully</source>
        <translation type="unfinished">Пайдаланушы туралы ақпарат сәтті жаңартылды</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="338"/>
        <source>Password updated successfully</source>
        <translation type="unfinished">Парольді жаңарту сәтті өтті</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="344"/>
        <source>The directory and files under the user&apos;s home directory are deleted with the user.Are you sure you want to delete the user(%1)?</source>
        <translation type="unfinished">Пайдаланушының үй каталогындағы каталогтар мен файлдар пайдаланушымен бірге жойылады. Пайдаланушыны өшіргіңіз келгені рас па (% 1)?</translation>
    </message>
    <message>
        <location filename="../../plugins/account/pages/user-info-page/user-info-page.cpp" line="347"/>
        <source>Warning</source>
        <translation type="unfinished">Ескерту</translation>
    </message>
</context>
<context>
    <name>VolumeIntputSubItem</name>
    <message>
        <location filename="../../plugins/audio/src/plugin/volume-input-subitem.h" line="32"/>
        <source>VolumeInput</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeOutputSubItem</name>
    <message>
        <location filename="../../plugins/audio/src/plugin/volume-output-subitem.h" line="32"/>
        <source>VolumeOutput</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeSettingPage</name>
    <message>
        <location filename="../../plugins/audio/src/system-tray/volume-setting-page.ui" line="35"/>
        <location filename="../../build/plugins/audio/kiran-audio-status-icon_autogen/include/ui_volume-setting-page.h" line="123"/>
        <source>VolumeSettingPage</source>
        <translation type="unfinished">VolumeSettingPage</translation>
    </message>
    <message>
        <location filename="../../plugins/audio/src/system-tray/volume-setting-page.ui" line="101"/>
        <location filename="../../build/plugins/audio/kiran-audio-status-icon_autogen/include/ui_volume-setting-page.h" line="124"/>
        <location filename="../../plugins/audio/src/system-tray/volume-setting-page.cpp" line="96"/>
        <source>Volume</source>
        <translation type="unfinished">Көлемі</translation>
    </message>
</context>
<context>
    <name>VpnIPsec</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="20"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="196"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="196"/>
        <source>VpnIPsec</source>
        <translation type="unfinished">VpnIPsec</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="43"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="197"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="197"/>
        <source>Enable IPsec</source>
        <translation type="unfinished">IPsec рұқсат етілсін</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="88"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="198"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="198"/>
        <source>Group Name</source>
        <translation type="unfinished">Топ атауы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="107"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="200"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="200"/>
        <source>EditGroupName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="121"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="202"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="202"/>
        <source>Group ID</source>
        <translation type="unfinished">&amp; Топ идентификаторы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="140"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="204"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="204"/>
        <source>EditGroupId</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="154"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="206"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="206"/>
        <source>Pre-Shared Key</source>
        <translation type="unfinished">Алдын ала ортақ кілт</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="175"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="208"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="208"/>
        <source>EditPreSharedKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="182"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="210"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="210"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="198"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="211"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="211"/>
        <source>Internet Key Exchange Protocol</source>
        <translation type="unfinished">Интернет-кілттермен алмасу протоколы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="217"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="213"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="213"/>
        <source>EditIpsecIKE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="231"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="215"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="215"/>
        <source>Encapsulating Security Payload</source>
        <translation type="unfinished">Пакеттік қауіпсіздік жүктемесі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="250"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipsec.h" line="217"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipsec.h" line="217"/>
        <source>EditIpsecESP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VpnIpvx</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="127"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="127"/>
        <source>VpnIpvx</source>
        <translation type="unfinished">VpnIpvx</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="37"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="128"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="128"/>
        <source>IPV4 Method</source>
        <translation type="unfinished">IPV4 әдісі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="56"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="130"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="130"/>
        <source>ComboBoxVPNIpv4Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="67"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="132"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="132"/>
        <source>Only applied in corresponding resources</source>
        <translation type="unfinished">Тек сәйкес ресурстарда қолданыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="97"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="133"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="133"/>
        <source>Preferred DNS</source>
        <translation type="unfinished">Қалаған DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="116"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="135"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="135"/>
        <source>EditVPNIpv4PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="133"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="137"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="137"/>
        <source>Alternate DNS</source>
        <translation type="unfinished">Күту режиміндегі DNS</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="152"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-ipvx.h" line="139"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ipvx.h" line="139"/>
        <source>EditIpv4AlternateDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.cpp" line="36"/>
        <source>Auto</source>
        <translation type="unfinished">Автоматты</translation>
    </message>
</context>
<context>
    <name>VpnL2tpSetting</name>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/vpn/vpn-l2tp-setting.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-l2tp-setting.h" line="100"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-l2tp-setting.h" line="100"/>
        <source>VpnL2tpSetting</source>
        <translation type="unfinished">VPNL 2TPS параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/vpn/vpn-l2tp-setting.cpp" line="27"/>
        <source>VPN name</source>
        <translation type="unfinished">VPN атауы</translation>
    </message>
</context>
<context>
    <name>VpnManager</name>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.ui" line="14"/>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.ui" line="17"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-manager.h" line="210"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-manager.h" line="212"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-manager.h" line="210"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-manager.h" line="212"/>
        <source>VpnManager</source>
        <translation type="unfinished">VpnManager</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.ui" line="133"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-manager.h" line="214"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-manager.h" line="214"/>
        <source>VPN type</source>
        <translation type="unfinished">VPN түрі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.ui" line="244"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-manager.h" line="215"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-manager.h" line="215"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.ui" line="285"/>
        <location filename="../../build/plugins/network/kiran-cpanel-network_autogen/include/ui_vpn-manager.h" line="216"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-manager.h" line="216"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.cpp" line="48"/>
        <source>VPN</source>
        <translation type="unfinished">Виртуалды жеке желі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.cpp" line="51"/>
        <source>L2TP</source>
        <translation type="unfinished">L2TP</translation>
    </message>
    <message>
        <source>PPTP</source>
        <translation type="obsolete">PPTP</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.cpp" line="200"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/vpn-manager.cpp" line="201"/>
        <source>Password required to connect to %1.</source>
        <translation type="unfinished">% 1 дегенге қосылу үшін пароль қажет.</translation>
    </message>
</context>
<context>
    <name>VpnPpp</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ppp.h" line="124"/>
        <source>VpnPpp</source>
        <translation type="unfinished">VpnPpp</translation>
    </message>
    <message>
        <source>VPN PPP</source>
        <translation type="obsolete">VPNPPP</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="37"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ppp.h" line="125"/>
        <source>Use MPPE</source>
        <translation type="unfinished">MPPE қолдану</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="85"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ppp.h" line="126"/>
        <source>Security</source>
        <translation type="unfinished">Қауіпсіздік</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="110"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ppp.h" line="128"/>
        <source>ComboBoxMppeSecurity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="121"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-ppp.h" line="130"/>
        <source>Stateful MPPE</source>
        <translation type="unfinished">Мемлекеттік MPPE</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="45"/>
        <source>All available (default)</source>
        <translation type="unfinished">Барлығы қол жетімді (әдепкі)</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="46"/>
        <source>40-bit (less secure)</source>
        <translation type="unfinished">40 бит (қауіпсіздігі төмен)</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="47"/>
        <source>128-bit (most secure)</source>
        <translation type="unfinished">128 бит (ең қауіпсіз)</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="91"/>
        <source>Refuse EAP Authentication</source>
        <translation type="unfinished">EAP аутентификациясынан бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="92"/>
        <source>Refuse PAP Authentication</source>
        <translation type="unfinished">PAP аутентификациясынан бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="93"/>
        <source>Refuse CHAP Authentication</source>
        <translation type="unfinished">CHAP аутентификациясынан бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="94"/>
        <source>Refuse MSCHAP Authentication</source>
        <translation type="unfinished">MSCHAP аутентификациясынан бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="95"/>
        <source>Refuse MSCHAPv2 Authentication</source>
        <translation type="unfinished">MSCHAPV2 аутентификациясынан бас тарту</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="96"/>
        <source>No BSD Data Compression</source>
        <translation type="unfinished">BSD деректерін сығымдау жоқ</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="97"/>
        <source>No Deflate Data Compression</source>
        <translation type="unfinished">Дефляция деректерін сығымдау жоқ</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="98"/>
        <source>No TCP Header Compression</source>
        <translation type="unfinished">TCP тақырыбын сығымдау жоқ</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="99"/>
        <source>No Protocol Field Compression</source>
        <translation type="unfinished">Протоколсыз өрісті сығымдау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="100"/>
        <source>No Address/Control Compression</source>
        <translation type="unfinished">Мекенжай/басқару қысылуы жоқ</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="101"/>
        <source>Send PPP Echo Packets</source>
        <translation type="unfinished">PPP жаңғырық пакетін жіберіңіз</translation>
    </message>
</context>
<context>
    <name>VpnPptpSetting</name>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/vpn/vpn-pptp-setting.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-pptp-setting.h" line="82"/>
        <source>VpnPptpSetting</source>
        <translation type="unfinished">VpnPptpSetting</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/vpn/vpn-pptp-setting.cpp" line="26"/>
        <source>VPN name</source>
        <translation type="unfinished">VPN атауы</translation>
    </message>
</context>
<context>
    <name>VpnWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="174"/>
        <source>VpnWidget</source>
        <translation type="unfinished">VpnWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="46"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="175"/>
        <source>Gateway</source>
        <translation type="unfinished">Шлюз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="65"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="177"/>
        <source>EditVPNGateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="85"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="179"/>
        <source>User Name</source>
        <translation type="unfinished">Пайдаланушы аты</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="104"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="181"/>
        <source>EditVPNUserName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="118"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="183"/>
        <source>Password Options</source>
        <translation type="unfinished">Пароль параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="140"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="185"/>
        <source>ComboBoxVPNPasswordOptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="167"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="187"/>
        <source>Password</source>
        <translation type="unfinished">Пароль</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="188"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="189"/>
        <source>EditVPNPassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="195"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="192"/>
        <source>ButtonPasswordVisual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="198"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="194"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="231"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="197"/>
        <source>EditNTDomain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PushButton</source>
        <translation type="obsolete">&amp; Түймешік</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="212"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_vpn-widget.h" line="195"/>
        <source>NT Domain</source>
        <translation type="unfinished">NT</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="28"/>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="29"/>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="30"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="40"/>
        <source>Saved</source>
        <translation type="unfinished">Сақталған</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="41"/>
        <source>Ask</source>
        <translation type="unfinished">&amp; Сұрау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="42"/>
        <source>Not required</source>
        <translation type="unfinished">Керегі жоқ</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="203"/>
        <source>Gateway can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="213"/>
        <source>Gateway invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="223"/>
        <source>user name can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="233"/>
        <source>password can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="14"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="179"/>
        <source>Form</source>
        <translation type="unfinished">Пішіні</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="68"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="180"/>
        <source>Set wallpaper</source>
        <translation type="unfinished">Түсқағазды орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="89"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="182"/>
        <source>FrameLockScreenPreview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="119"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="185"/>
        <source>FrameDesktopPreivew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="143"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="187"/>
        <source>Desktop Wallpaper Preview</source>
        <translation type="unfinished">Жұмыс үстелінің тұсқағазын алдын-ала қарау</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="153"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="188"/>
        <source>Lock Screen WallPaper Preview</source>
        <translation type="unfinished">Құлыпталған экранның тұсқағазын алдын ала қарау</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="172"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="189"/>
        <source>Select wallpaper</source>
        <translation type="unfinished">Түсқағазды таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.ui" line="226"/>
        <location filename="../../build/plugins/appearance/kiran-cpanel-appearance_autogen/include/ui_wallpaper.h" line="190"/>
        <source>Select Wallpaper</source>
        <translation type="unfinished">Түсқағазды таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="108"/>
        <source>Set Desktop Wallpaper</source>
        <translation type="unfinished">Жұмыс үстелінің тұсқағазын орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="124"/>
        <source>Set Lock Screen Wallpaper</source>
        <translation type="unfinished">Құлыптау экранының тұсқағазын орнатыңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="164"/>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="176"/>
        <source>set wallpaper</source>
        <translation type="unfinished">Түсқағазды орнату</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="164"/>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="176"/>
        <source>Set wallpaper failed!</source>
        <translation type="unfinished">Түсқағазды орнату сәтсіз аяқталды!</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="187"/>
        <source>select picture</source>
        <translation type="unfinished">Суретті таңдау</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="189"/>
        <source>image files(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</source>
        <translation type="unfinished">Сурет файлы (*. bmp *. jpg *. png *. tif *. gif *. tga *. exif *. fpx *. svg *. pcd *. dxf *. ufo *. eps *. ai *. raw *. WMF *. webp)</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="209"/>
        <source>Add Image Failed</source>
        <translation type="unfinished">Кескінді қосу жаңылысы</translation>
    </message>
    <message>
        <location filename="../../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="210"/>
        <source>The image already exists!</source>
        <translation type="unfinished">Сурет бұрыннан бар!</translation>
    </message>
</context>
<context>
    <name>WiredManager</name>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.ui" line="14"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_wired-manager.h" line="149"/>
        <source>WiredManager</source>
        <translation type="unfinished">Кабель менеджеріName</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.ui" line="152"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_wired-manager.h" line="151"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.ui" line="155"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_wired-manager.h" line="153"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.ui" line="196"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_wired-manager.h" line="155"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.ui" line="199"/>
        <location filename="../../build/plugins/network/kiran-network-status-icon_autogen/include/ui_wired-manager.h" line="157"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.cpp" line="45"/>
        <source>Wired Network Adapter</source>
        <translation type="unfinished">Сымды желі адаптері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.cpp" line="111"/>
        <source>The carrier is pulled out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wired-manager.cpp" line="96"/>
        <source>The current device is not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WiredSettingPage</name>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/wired-setting-page.ui" line="14"/>
        <source>WiredSettingPage</source>
        <translation type="unfinished">Виредседин беті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/wired-setting-page.cpp" line="75"/>
        <source>Network name</source>
        <translation type="unfinished">Желі атауы</translation>
    </message>
</context>
<context>
    <name>WirelessManager</name>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.ui" line="14"/>
        <source>WirelessManager</source>
        <translation type="unfinished">Сымсыз менеджер</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.ui" line="149"/>
        <source>Save</source>
        <translation type="unfinished">&amp; Сақтау</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.ui" line="190"/>
        <source>Return</source>
        <translation type="unfinished">Қайтару</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.cpp" line="47"/>
        <source>Wireless Network Adapter</source>
        <translation type="unfinished">Сымсыз желі адаптері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.cpp" line="118"/>
        <source>The current device is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.cpp" line="376"/>
        <source>Tips</source>
        <translation type="unfinished">Кеңестер</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/manager/wireless-manager.cpp" line="377"/>
        <source>Password required to connect to %1.</source>
        <translation type="unfinished">% 1 дегенге қосылу үшін пароль қажет.</translation>
    </message>
</context>
<context>
    <name>WirelessSecurityWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="14"/>
        <source>WirelessSecurityWidget</source>
        <translation type="unfinished">Сымсыз SecurityWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="40"/>
        <source>Security</source>
        <translation type="unfinished">Қауіпсіздік</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="59"/>
        <source>ComboBoxWirelessSecurityOption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="92"/>
        <source>Password Options</source>
        <translation type="unfinished">Пароль параметрлері</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="111"/>
        <source>ComboBoxWirelessPasswordOption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="138"/>
        <source>Password</source>
        <translation type="unfinished">Пароль</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="159"/>
        <source>EditWirelessPassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="166"/>
        <source>ButtonWirelessPasswordVisual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="169"/>
        <source>PushButton</source>
        <translation type="unfinished">&amp; Түймешік</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="36"/>
        <source>None</source>
        <translation type="unfinished">Ешқайсысы</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="37"/>
        <source>WPA/WPA2 Personal</source>
        <translation type="unfinished">WPA/WPA 2 адам</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="41"/>
        <source>Save password for all users</source>
        <translation type="unfinished">Барлық пайдаланушылардың құпия сөздерін сақтаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="42"/>
        <source>Save password for this user</source>
        <translation type="unfinished">Осы пайдаланушының паролін сақтаңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="43"/>
        <source>Ask me always</source>
        <translation type="unfinished">Әрқашан мені сұраңыз</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="45"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
</context>
<context>
    <name>WirelessSettingPage</name>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/wireless-setting-page.ui" line="14"/>
        <source>WirelessSettingPage</source>
        <translation type="unfinished">Сымсыз параметрлер беті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/settings/wireless-setting-page.cpp" line="66"/>
        <source>Wireless name</source>
        <translation type="unfinished">Сымсыз атауы</translation>
    </message>
</context>
<context>
    <name>WirelessTrayWidget</name>
    <message>
        <location filename="../../plugins/network/src/tray/wireless-tray-widget.cpp" line="491"/>
        <source>the network &quot;%1&quot; not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WirelessWidget</name>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="14"/>
        <source>WirelessWidget</source>
        <translation type="unfinished">WirelessWidget</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="40"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="59"/>
        <source>EditSsid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="76"/>
        <source>MAC Address Of Device</source>
        <translation type="unfinished">Құрылғының MAC адресі</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="95"/>
        <source>ComboBoxWirelessMacAddress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="111"/>
        <source>Custom MTU</source>
        <translation type="unfinished">Қалаған MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="145"/>
        <source>SpinBoxWirelessCustomMTU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.cpp" line="37"/>
        <source>Required</source>
        <translation type="unfinished">Қажетті</translation>
    </message>
    <message>
        <location filename="../../plugins/network/src/plugin/setting-widget/wireless-widget.cpp" line="46"/>
        <source>No device specified</source>
        <translation type="unfinished">Құрылғы көрсетілмеген</translation>
    </message>
</context>
<context>
    <name>YearSpinBox</name>
    <message>
        <location filename="../../plugins/timedate/widgets/date-spinbox.h" line="35"/>
        <source>yyyy</source>
        <translation type="unfinished">Philippines. kgm</translation>
    </message>
</context>
</TS>
