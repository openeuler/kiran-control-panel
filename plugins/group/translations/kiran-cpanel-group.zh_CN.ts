<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddUsersPage</name>
    <message>
        <location filename="../src/pages/add-users-page/add-users-page.cpp" line="49"/>
        <source>Please input keys for search...</source>
        <translation>请输入搜索关键词...</translation>
    </message>
    <message>
        <location filename="../src/pages/add-users-page/add-users-page.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../src/pages/add-users-page/add-users-page.ui" line="40"/>
        <source>Add Member</source>
        <translation>添加组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/add-users-page/add-users-page.ui" line="120"/>
        <source>Save</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/pages/add-users-page/add-users-page.ui" line="167"/>
        <source>Cancel</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>CreateGroupPage</name>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="20"/>
        <source>CreateGroupPage</source>
        <translation>创建用户组</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="46"/>
        <source>Create Group</source>
        <translation>创建组</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="76"/>
        <source>Add Group Members</source>
        <translation>添加组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="128"/>
        <source>Confirm</source>
        <translation>创建</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">返回</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="105"/>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="135"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>GroupInfoPage</name>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">TextLabel</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="175"/>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="269"/>
        <source>Member List</source>
        <translation>组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="327"/>
        <source>Add User</source>
        <translation>添加成员</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">返回</translation>
    </message>
    <message>
        <source>Add Member</source>
        <translation type="vanished">添加组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="374"/>
        <source>Delete</source>
        <translation>删除组</translation>
    </message>
    <message>
        <source>Please input keys for search...</source>
        <translation type="vanished">请输入搜索关键词...</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="160"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="171"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="183"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="192"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>GroupInterface</name>
    <message>
        <location filename="../src/group-interface.cpp" line="45"/>
        <source>Failed to create group, %1</source>
        <translation>创建用户组失败， %1</translation>
    </message>
    <message>
        <location filename="../src/group-interface.cpp" line="69"/>
        <source>Failed to delete group, %1</source>
        <translation>删除用户组失败， %1</translation>
    </message>
    <message>
        <location filename="../src/group-interface.cpp" line="92"/>
        <source>Failed to add %1 to group, %2</source>
        <translation>添加用户%1到用户组失败， %2</translation>
    </message>
    <message>
        <location filename="../src/group-interface.cpp" line="114"/>
        <source>Failed to remove %1 from group, %2</source>
        <translation>从用户组移除用户%1失败， %2</translation>
    </message>
    <message>
        <location filename="../src/group-interface.cpp" line="138"/>
        <source>Failed to change group name to %1, %2</source>
        <translation>修改用户组名为%1失败，%2</translation>
    </message>
    <message>
        <location filename="../src/group-interface.cpp" line="150"/>
        <source>Failed to change group name to %1, the new group name is occupied!</source>
        <translation>修改用户组名为%1失败，新名称已被使用！</translation>
    </message>
</context>
<context>
    <name>GroupNameChecker</name>
    <message>
        <location filename="../src/tools/group-name-checker.cpp" line="24"/>
        <source>Please enter your group name</source>
        <translation>请输入组名</translation>
    </message>
    <message>
        <location filename="../src/tools/group-name-checker.cpp" line="33"/>
        <source>Group name cannot be a pure number</source>
        <translation>组名不能为纯数字</translation>
    </message>
    <message>
        <location filename="../src/tools/group-name-checker.cpp" line="40"/>
        <source>Group name already exists</source>
        <translation>组名已经存在</translation>
    </message>
    <message>
        <source>group name cannot be a pure number</source>
        <translation type="vanished">组名不能为纯数字</translation>
    </message>
    <message>
        <source>group name already exists</source>
        <translation type="vanished">组名已经存在</translation>
    </message>
</context>
<context>
    <name>GroupPage</name>
    <message>
        <location filename="../src/group-page.cpp" line="141"/>
        <source>Create new group</source>
        <translation>创建组</translation>
    </message>
</context>
<context>
    <name>GroupSubItem</name>
    <message>
        <location filename="../src/group-subitem.cpp" line="49"/>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <location filename="../src/group-subitem.cpp" line="81"/>
        <source>Create group</source>
        <translation>创建用户组</translation>
    </message>
    <message>
        <location filename="../src/group-subitem.cpp" line="82"/>
        <source>Group information</source>
        <translation>用户组信息</translation>
    </message>
    <message>
        <source>Creat group</source>
        <translation type="vanished">创建组</translation>
    </message>
    <message>
        <source>Change group name</source>
        <translation type="vanished">修改用户组名</translation>
    </message>
    <message>
        <source>Add group member</source>
        <translation type="vanished">添加用户组成员</translation>
    </message>
</context>
<context>
    <name>HardWorker</name>
    <message>
        <source>Create Group failed</source>
        <translation type="vanished">创建组失败</translation>
    </message>
    <message>
        <source>Failed to delete group,%1</source>
        <translation type="vanished">删除组失败，%1</translation>
    </message>
    <message>
        <source> add user to group failed</source>
        <translation type="vanished">添加用户到组失败</translation>
    </message>
    <message>
        <source> change group name failed</source>
        <translation type="vanished">修改组名失败</translation>
    </message>
    <message>
        <source> change group name failed, the new group name is occupied</source>
        <translation type="vanished">修改组名失败，新组名已存在</translation>
    </message>
    <message>
        <source>Failed to create group, %1</source>
        <translation type="vanished">创建用户组失败， %1</translation>
    </message>
    <message>
        <source>Failed to delete group, %1</source>
        <translation type="vanished">删除用户组失败， %1</translation>
    </message>
    <message>
        <source>Failed to add %1 to group, %2</source>
        <translation type="vanished">添加用户%1到用户组失败， %2</translation>
    </message>
    <message>
        <source>Failed to remove %1 from group, %2</source>
        <translation type="vanished">从用户组移除用户%1失败， %2</translation>
    </message>
    <message>
        <source>Failed to change group name to %1, %2</source>
        <translation type="vanished">修改用户组名为%1失败，%2</translation>
    </message>
    <message>
        <source>Failed to change group name to %1, the new group name is occupied!</source>
        <translation type="vanished">修改用户组名为%1失败，新名称已被使用！</translation>
    </message>
</context>
<context>
    <name>KiranGroupManager</name>
    <message>
        <source>Create new group</source>
        <translation type="vanished">创建组</translation>
    </message>
</context>
</TS>
