<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../src/device-panel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="122"/>
        <source>Rotate left 90 degrees</source>
        <translation>左旋转90度</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="125"/>
        <source>ButtonLeft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="156"/>
        <source>Rotate right 90 degrees</source>
        <translation>右旋转90度</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="159"/>
        <source>ButtonRight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="190"/>
        <source>Turn left and right</source>
        <translation>左右翻转</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="193"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="227"/>
        <source>upside down</source>
        <translation>上下翻转</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="230"/>
        <source>ButtonVertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="264"/>
        <source>Identification display</source>
        <translation>标识显示器</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="267"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../src/display-page.ui" line="14"/>
        <source>DisplayPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="110"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="134"/>
        <source>Copy display</source>
        <translation>复制显示</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="162"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="186"/>
        <source>Extended display</source>
        <translation>扩展显示</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="289"/>
        <location filename="../src/display-page.ui" line="562"/>
        <source>Resolution ratio</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="308"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="325"/>
        <location filename="../src/display-page.ui" line="598"/>
        <source>Refresh rate</source>
        <translation>刷新率</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="344"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="358"/>
        <location filename="../src/display-page.ui" line="631"/>
        <source>Zoom rate</source>
        <translation>缩放率</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="377"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="381"/>
        <location filename="../src/display-page.ui" line="654"/>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="386"/>
        <location filename="../src/display-page.ui" line="659"/>
        <source>100% (recommended)</source>
        <translation>100% (推荐)</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="391"/>
        <location filename="../src/display-page.ui" line="664"/>
        <source>200%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="462"/>
        <source>Open</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="514"/>
        <source>Set as main display</source>
        <translation>设为主显示器</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="540"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="581"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="617"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="650"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="731"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="737"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="756"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="762"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="255"/>
        <location filename="../src/display-page.cpp" line="279"/>
        <source> (recommended)</source>
        <translation> (推荐)</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="351"/>
        <source>Is the display normal?</source>
        <translation>显示是否正常?</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="354"/>
        <source>Save current configuration(K)</source>
        <translation>保存当前配置(K)</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="358"/>
        <source>Restore previous configuration(R)</source>
        <translation>恢复之前的配置(R)</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="366"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation>显示将会在 %1 秒后恢复之前的配置</translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation>显示</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/display-config.cpp" line="466"/>
        <location filename="../src/display-config.h" line="134"/>
        <location filename="../src/display-page.cpp" line="387"/>
        <location filename="../src/display-page.cpp" line="405"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../src/display-config.cpp" line="469"/>
        <location filename="../src/display-config.h" line="137"/>
        <location filename="../src/display-page.cpp" line="390"/>
        <location filename="../src/display-page.cpp" line="408"/>
        <source>OK(K)</source>
        <translation>确定(K)</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="394"/>
        <source>Failed to apply display settings!%1</source>
        <translation>应用显示设置失败!%1</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="412"/>
        <source>Fallback display setting failed! %1</source>
        <translation>回撤显示设置失败! %1</translation>
    </message>
</context>
</TS>
