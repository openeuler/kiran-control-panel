<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">ཙིག་རྟགས་དང་རེག་པང་</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">ཙིག་རྟགས་བདམས་ནས་ཕྱོགས་སྟོན་།</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">ཙིག་རྟགས་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">རང་བྱུང་འཁྱིལ་འཁོར་</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">བར་གྱི་འདྲ་བཟོ་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">རེག་པང་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">རེག་པང་ལག་པ་བདམས་།</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">པང་རེག་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">ཆིག་རྡེབ་ཐབས་བདམས་།</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">འགྲིལ་སྟངས་འདེམས་པ།</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">མཐེབ་འཇུག་དུས་སྤྱོད་པ།</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">གནོན་ནས་མནན་ན་</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">གྱོད་དཔེ།</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">ཕྱིར་གཏོང་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">ཙིག་རྟགས་སྒྲིག་འགོད་</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">རེག་པང་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">གཡས་ལག་རྣམ་པ་</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">ལག་གཡོན་མའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">མར་མནན་ནས་གནོན་དང་།</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">འཐེན་མགོ་</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">མཛུབ་ཟུང་སྒྲིལ་མདའ།</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">མུ་འགྲམ་སྒྲིལ་མདའ།</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">དལ་པོ།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར་ཅན།</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">རྣམ་པ་</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>ཙིག་རྟགས་བདམས་ནས་ཕྱོགས་སྟོན་།</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་འདེམས་ཙིག་ལག་</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>ཙིག་རྟགས་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished">SliderMozeMotonAccleeron</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>དལ་པོ།</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>རང་བྱུང་འཁྱིལ་འཁོར་</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished">SwitchMoueNatturalScroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>བར་གྱི་འདྲ་བཟོ་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished">SwitchMidleEmulomon</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>བྱི་བའི་འཁོར་ལོའི་ཁ་ཕྱོགས་ལ་ཚོད</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་དང་པོ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་གཉིས་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་གསུམ་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཞི་བ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་5་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ནང་གི་ཐིག་དྲུག་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་7པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་8པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་9པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་བ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་གཅིག་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་གཉིས་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་གསུམ་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་བཞི་བ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅོ་ལྔ་བ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་དྲུག་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་བདུན་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅོ་བརྒྱད་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་བཅུ་དགུ་བ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་20་་་
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་21་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་22་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་23པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་24པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་25་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་26པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་ཉེར་བདུན་པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་28པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་29པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་30པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་31པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་32པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་33པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་34པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་35པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་36པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་37པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་38པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་39པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཐིག་40ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་41པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་42པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་43པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་44པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་45པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་46པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་47པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་48པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་49པ་ཡིན།
འདི་ནི་རྒྱུགས་ལེན་ཡི་གེའི་ཨང་50་་་</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>གཡས་ལག་རྣམ་པ་</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>ལག་གཡོན་མའི་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">ཙིག་རྟགས་བདམས་ནས་ཕྱོགས་སྟོན་།</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">ཙིག་རྟགས་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">རང་བྱུང་འཁྱིལ་འཁོར་</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">བར་གྱི་འདྲ་བཟོ་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">གཡས་ལག་རྣམ་པ་</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">ལག་གཡོན་མའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">དལ་པོ།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར་ཅན།</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>ཙིག་རྟགས་སྒྲིག་འགོད་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">དལ་པོ།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར་ཅན།</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">ཕམ་པ།</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">ཙིག་རྟགས་དང་རེག་པང་Dbusའབྲེལ་ནས་ཕམ་པ།</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">ཐ་ཚིག་</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">QQ ssབླུགས་པའི་ཡིག་ཆ་དེ་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">རྣམ་པ་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>རེག་པང་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished">SwitchTouchPadEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>རེག་པང་ལག་པ་བདམས་།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་ལག་པས་རེག་།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>པང་རེག་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished">འདྲེད་རེག་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>དལ་པོ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>ཆིག་རྡེབ་ཐབས་བདམས་།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished">བུ་སྣོད་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>འགྲིལ་སྟངས་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་འགྲིལ་ཐབས།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>རང་བྱུང་འཁྱིལ་འཁོར་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་རང་བྱུང་སྒྲིལ་མདའ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>མཐེབ་འཇུག་དུས་སྤྱོད་པ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished">SwitchTypingEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>གནོན་ནས་མནན་ན་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished">སི་ཝེ་ཏེ་ཧོ་·ཐ་ཕུའུ་ཐུའོ་ཁེ་ལི་ཁེ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>གཡས་ལག་རྣམ་པ་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>ལག་གཡོན་མའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>མར་མནན་ནས་གནོན་དང་།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>འཐེན་མགོ་</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>མཛུབ་ཟུང་སྒྲིལ་མདའ།</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>མུ་འགྲམ་སྒྲིལ་མདའ།</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">རེག་པང་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">Disable TouchPad་</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">རེག་པང་སྤྱད་ཟིན་།</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">རེག་པང་ལག་པ་བདམས་།</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">པང་རེག་འགུལ་སྐྱོད་འགྲོས་སྣོན་ཚད།</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">ཆིག་རྡེབ་ཐབས་བདམས་།</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">འགྲིལ་སྟངས་འདེམས་པ།</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">རང་བྱུང་འཁྱིལ་འཁོར་</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">མཐེབ་འཇུག་དུས་སྤྱོད་པ།</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">གནོན་ནས་མནན་ན་</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">དལ་པོ།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར་ཅན།</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">གཡས་ལག་རྣམ་པ་</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">ལག་གཡོན་མའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">མར་མནན་ནས་གནོན་དང་།</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">འཐེན་མགོ་</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">མཛུབ་ཟུང་སྒྲིལ་མདའ།</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">མུ་འགྲམ་སྒྲིལ་མདའ།</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>རེག་པང་སྒྲིག་བཀོད།</translation>
    </message>
</context>
</TS>
