<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">مائۇس ۋە TouchPad</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">چاشقان قول تاللاش</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">مائۇس ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">تەبىئىي سىيرىلما</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">ئوتتۇرا تەقلىدلەش ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad قول تاللاش</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">چېكىش ئۇسۇلىنى تاللاڭ</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">سىيرىلما ئۇسۇلىنى تاللاش</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">خەت باسقاندا قوزغىتىش</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">چېكىش ئۈچۈن چېكىڭ</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">قايتا تىڭشى</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">چىقىش ئېغىزى</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">ساقلاش</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">مائۇس تەڭشەكلىرى</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">TouchPad تەڭشەكلىرى</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ئۆلچەم</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">ئوڭ قول ھالىتى</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">سول قول ھالىتى</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">بېسىش ۋە بېسىپ بېسىپ</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Tap</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">ئىككى بارماق سىيرىلما</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge Scroll</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">ئاستا</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">تاسقاپ</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">جەدۋەل</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>چاشقان قول تاللاش</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished">ComboSelectMouseHand</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>مائۇس ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished">SliderMouseMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>ئاستا</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>تاسقاپ</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>تەبىئىي سىيرىلما</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished">SwitchMouseNatturalScroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>ئوتتۇرا تەقلىدلەش ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished">SwitchMiddleEmulation</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>تېستموس خۇير دىلېكېن</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>بۇ بىرىنچى قۇر سىناق يېزىقى
بۇ ئىككىنچى قۇر سىناق يېزىقى
بۇ ئۈچىنچى قۇر سىناق يېزىقى
بۇ تۆتىنچى قۇر سىناق يېزىقى
بۇ بەشىنچى قۇر سىناق يېزىقى
بۇ ئالتىنچى قۇر سىناق يېزىقى
بۇ يەتتىنچى قۇر سىناق يېزىقى
بۇ سەككىزىنچى قۇر سىناق يېزىقى
بۇ توققۇزىنچى قۇر سىناق يېزىقى
بۇ 10-قېتىملىق سىناق يېزىقى
بۇ 11-قۇر سىناق يېزىقى
بۇ 12-قۇر سىناق يېزىقى
بۇ 13-قۇر سىناق يېزىقى
بۇ 14-قۇر سىناق يېزىقى
بۇ 15-قۇر سىناق يېزىقى
بۇ 16-قۇر سىناق يېزىقى
بۇ 17-قۇر سىناق يېزىقى
بۇ 18-قۇر سىناق يېزىقى
بۇ 19-قۇر سىناق يېزىقى
بۇ 20-قېتىملىق سىناق يېزىقى
بۇ 21-قۇر سىناق خېتى
بۇ 22-قۇر سىناق يېزىقى
بۇ 23-قۇر سىناق يېزىقى
بۇ 24-قۇر سىناق يېزىقى
بۇ 25-قېتىملىق سىناق يېزىقى
بۇ 26-قۇر سىناق يېزىقى
بۇ 27-قېتىملىق سىناق يېزىقى
بۇ 28-قۇر سىناق يېزىقى
بۇ 29-قۇر سىناق يېزىقى
بۇ 30-قېتىملىق سىناق يېزىقى
بۇ 31-قۇر سىناق يېزىقى
بۇ 32-قۇر سىناق يېزىقى
بۇ 33- قۇر سىناق يېزىقى
بۇ 34-قۇر سىناق يېزىقى
بۇ 35-قۇر سىناق يېزىقى
بۇ 36-قۇر سىناق يېزىقى
بۇ 37-قۇر سىناق يېزىقى
بۇ 38-قۇر سىناق يېزىقى
بۇ 39-قۇر سىناق يېزىقى
بۇ 40-قېتىملىق سىناق يېزىقى
بۇ 41-قۇر سىناق يېزىقى
بۇ 42-قۇر سىناق يېزىقى
بۇ 43- قۇر سىناق يېزىقى
بۇ 44-قۇر سىناق يېزىقى
بۇ 45-قۇر سىناق يېزىقى
بۇ 46-قۇر سىناق يېزىقى
بۇ 47-قۇر سىناق يېزىقى
بۇ 48-قۇر سىناق يېزىقى
بۇ 49-قۇر سىناق يېزىقى
بۇ 50-قېتىملىق سىناق يېزىقى</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>ئوڭ قول ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>سول قول ھالىتى</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">چاشقان قول تاللاش</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">مائۇس ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">تەبىئىي سىيرىلما</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">ئوتتۇرا تەقلىدلەش ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">ئوڭ قول ھالىتى</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">سول قول ھالىتى</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">ئاستا</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ئۆلچەم</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">تاسقاپ</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>مائۇس تەڭشەكلىرى</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">Slow</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ئۆلچەم</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">تاسقاپ</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">مائۇسنى ئۇلاش ياكى TouchPad Dbus نى ئۇلاش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">qss ھۆججىتىنى قاچىلاش مەغلۇپ بولدى!</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">جەدۋەل</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>TouchPad ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished">SwitchTouchPadEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>TouchPad قول تاللاش</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished">ComboTouchPadHand</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>TouchPad ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished">SliderTouchPadMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>ئاستا</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>تاسقاپ</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>چېكىش ئۇسۇلىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished">ComboClickMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>سىيرىلما ئۇسۇلىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished">ComboScrollMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>تەبىئىي سىيرىلما</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished">ComboNaturalScroll</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>خەت باسقاندا قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished">SwitchTypingEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>چېكىش ئۈچۈن چېكىڭ</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished">SwtichTapToClick</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>ئوڭ قول ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>سول قول ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>بېسىش ۋە بېسىپ بېسىپ</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>Tap</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>ئىككى بارماق سىيرىلما</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>Edge Scroll</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">TouchPad نى بىكار قىلىش</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">TouchPad ئىقتىدارى قوشۇلدى</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad قول تاللاش</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad ھەرىكەت تېزلىتىش</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">چېكىش ئۇسۇلىنى تاللاڭ</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">سىيرىلما ئۇسۇلىنى تاللاش</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">تەبىئىي سىيرىلما</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">خەت باسقاندا قوزغىتىش</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">چېكىش ئۈچۈن چېكىڭ</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">ئاستا</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">ئۆلچەم</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">تاسقاپ</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">ئوڭ قول ھالىتى</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">سول قول ھالىتى</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">بېسىش ۋە بېسىپ بېسىپ</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Tap</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">ئىككى بارماق سىيرىلما</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge Scroll</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>TouchPad تەڭشەكلىرى</translation>
    </message>
</context>
</TS>
