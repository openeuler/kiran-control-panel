<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">Чычкан жана TouchPad</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Чычкан колун тандаңыз</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Чычкан кыймылынын ылдамдашы</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Natural Scroll</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Орто эмуляция иштетилген</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad иштетилген</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad колун тандаңыз</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad кыймыл ылдамдашы</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">чыкылдатуу методу</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Жылдыруу ыкмасын тандаңыз</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Терүү учурунда иштетилген</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">Чыкылдатуу үчүн таптаңыз</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">баштапкы абалга келтирүү</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">чыгуу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">жокко чыгаруу</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">сактоо</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">Чычкан жөндөөлөрү</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">TouchPad Жөндөөлөрү</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">стандарт</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң кол режими</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол кол режими</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Басуу жана таптоо</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">таптоо</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Эки манжа түрмөгү</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge Scroll</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">жай</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">тез</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">форма</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>Чычкан колун тандаңыз</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished">ComboSelectMouseHand</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>Чычкан кыймылынын ылдамдашы</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished">SliderMouseMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>жай</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>тез</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>Natural Scroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished">SwitchMouseNatturalScroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>Орто эмуляция иштетилген</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished">SwitchMiddleEmulation</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>Сыноо чычкан дөңгөлөк багыты</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>Бул сыноо текстинин 1-сап
Бул сыноо текстинин 2-сап
Бул сыноо текстинин 3-сап
Бул сыноо текстинин 4-сызыгы
Бул сыноо текстинин 5-сап
Бул сыноо текстинин 6-сызыгы
Бул сыноо текстинин 7-сап
Бул сыноо текстинин 8-сап
Бул сыноо текстинин 9-сызыгы
Бул сыноо текстинин 10 сап
Бул сыноо текстинин 11 сап
Бул сыноо текстинин 12 сап
Бул сыноо текстинин 13 сап
Бул сыноо текстинин 14 сап
Бул сыноо текстинин 15 сап
Бул сыноо текстинин 16 сап
Бул сыноо текстинин 17 сап
Бул сыноо текстинин 18 сап
Бул сызык 19 сыноо тексти
Бул сызык 20 сыноо тексти
Бул сыноо текстинин 21 сап
Бул сыноо текстинин 22 сап
Бул сыноо текстинин 23 сап
Бул сыноо текстинин 24 сап
Бул сыноо текстинин 25 сап
Бул сыноо текстинин 26 сап
Бул сыноо текстинин 27 сап
Бул сызык 28 сыноо тексти
Бул сыноо текстинин 29 сап
Бул сыноо текстинин 30 сап
Бул сыноо текстинин 31 сап
Бул сыноо текстинин 32 сап
Бул сыноо текстинин 33 сап
Бул сыноо текстинин 34 сап
Бул сыноо текстинин 35 сап
Бул сыноо текстинин 36 сап
Бул сыноо текстинин 37 сап
Бул сыноо текстинин 38 сап
Бул сыноо текстинин 39 сап
Бул сыноо текстинин 40 сап
Бул сыноо текстинин 41 сап
Бул сыноо текстинин 42 сап
Бул сыноо текстинин 43 сап
Бул сыноо текстинин 44 сап
Бул сыноо текстинин 45 сап
Бул сыноо текстинин 46 сап
Бул сыноо текстинин 47 сап
Бул сыноо текстинин 48 сап
Бул сыноо текстинин 49 сап
Бул сыноо текстинин 50 сап</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>Оң кол режими</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>Сол кол режими</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Чычкан колун тандаңыз</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Чычкан кыймылынын ылдамдашы</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Natural Scroll</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Орто эмуляция иштетилген</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң кол режими</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол кол режими</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">жай</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">тез</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>Чычкан жөндөөлөрү</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">жай</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">тез</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">Faild</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">Чычканды же TouchPad Dbus туташтырылды!</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">эскертүү</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">qss файлын жүктөө ишке ашкан жок!</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">форма</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>TouchPad иштетилген</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished">SwitchTouchPadEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>TouchPad колун тандаңыз</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished">ComboTouchPadHand</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>TouchPad кыймыл ылдамдашы</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished">SliderTouchPadMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>жай</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>тез</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>чыкылдатуу методу</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished">ComboClickMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>Жылдыруу ыкмасын тандаңыз</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished">ComboScrollMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>Natural Scroll</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished">ComboNaturalScroll</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>Терүү учурунда иштетилген</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished">SwitchTyping иштетүү</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>Чыкылдатуу үчүн таптаңыз</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished">SwtichTapToClick</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>Оң кол режими</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>Сол кол режими</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>Басуу жана таптоо</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>таптоо</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>Эки манжа түрмөгү</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>Edge Scroll</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad иштетилген</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">TouchPad өчүрүү</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">TouchPad иштетилген</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad колун тандаңыз</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad кыймыл ылдамдашы</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">чыкылдатуу методу</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Жылдыруу ыкмасын тандаңыз</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Natural Scroll</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Терүү учурунда иштетилген</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">Чыкылдатуу үчүн таптаңыз</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">жай</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">тез</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң кол режими</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол кол режими</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Басуу жана таптоо</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">таптоо</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Эки манжа түрмөгү</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge Scroll</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>TouchPad Жөндөөлөрү</translation>
    </message>
</context>
</TS>
