<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">Хулгана ба TouchPad</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Хулганы гарыг сонгоно уу</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Хулганы хөдөлгөөн</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Байгалийн гүйлгэх</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Дунд зэргийн эмуляцийг идэвхжүүлсэн</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad идэвхжүүлсэн</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad гарыг сонгоно уу</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad Motion хурд</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">Арга сонгоно уу</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Урвах аргыг сонгоно уу</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Тайрах үед идэвхждэг</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">товш</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Дахин тохируулах</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Гарах</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Цуцлах</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Хадгалах</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">Хулганы тохиргоо</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">TouchPad тохиргоо</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Баруун гар горим</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Зүүн гар</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Хэвлэлийн</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Та</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Хоёр хурууны гүйлгэх</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Удаан</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Хурдан</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Маягт</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>Хулганы гарыг сонгоно уу</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished">ComboSelectMouseHand</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>Хулганы хөдөлгөөн</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished">СлайдерMouseMotionAcrez</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>Удаан</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>Хурдан</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>Байгалийн гүйлгэх</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished">SwitchMouseNatturalScroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>Дунд зэргийн эмуляцийг идэвхжүүлсэн</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished">SwitchMiddleEmulation</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>Хулганы дугуйны чиглэлийг шалгах</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>Энэ бол туршилтын текстийн эхний мөр юм
Энэ бол туршилтын текстийн хоёр дахь мөр юм
Энэ бол туршилтын текстийн 3-р мөр юм
Энэ бол туршилтын текстийн 4-р мөр юм
Энэ бол туршилтын текстийн 5-р мөр юм
Энэ бол туршилтын текстийн 6-р мөр юм
Энэ бол туршилтын текстийн 7-р мөр юм
Энэ бол туршилтын текстийн 8-р мөр юм
Энэ бол туршилтын текстийн 9-р мөр юм
Энэ бол туршилтын текстийн 10-р мөр юм
Энэ бол туршилтын текстийн 11-р мөр юм
Энэ бол туршилтын текстийн 12-р мөр юм
Энэ бол туршилтын текстийн 13-р мөр юм
Энэ бол туршилтын текстийн 14-р мөр юм
Энэ бол туршилтын текстийн 15-р мөр юм
Энэ бол туршилтын текстийн 16-р мөр юм
Энэ бол туршилтын текстийн 17-р мөр юм
Энэ бол туршилтын текстийн 18-р мөр юм
Энэ бол туршилтын текстийн 19-р мөр юм
Энэ бол туршилтын текстийн 20-р мөр юм
Энэ бол туршилтын текстийн 21-р мөр юм
Энэ бол туршилтын текстийн 22-р мөр юм
Энэ бол туршилтын текстийн 23-р мөр юм
Энэ бол туршилтын текстийн 24-р мөр юм
Энэ бол туршилтын текстийн 25-р мөр юм
Энэ бол туршилтын текстийн 26-р мөр юм
Энэ бол туршилтын текстийн 27-р мөр юм
Энэ бол туршилтын текстийн 28-р мөр юм
Энэ бол туршилтын текстийн 29-р мөр юм
Энэ бол туршилтын текстийн 30-р мөр юм
Энэ бол туршилтын текстийн 31-р мөр юм
Энэ бол туршилтын текстийн 32-р мөр юм
Энэ бол туршилтын текстийн 33-р мөр юм
Энэ бол туршилтын текстийн 34-р мөр юм
Энэ бол туршилтын текстийн 35-р мөр юм
Энэ бол туршилтын текстийн 36-р мөр юм
Энэ бол туршилтын текстийн 37-р мөр юм
Энэ бол туршилтын текстийн 38-р мөр юм
Энэ бол туршилтын текстийн 39-р мөр юм
Энэ бол туршилтын текстийн 40-р мөр юм
Энэ бол туршилтын текстийн 41-р мөр юм
Энэ бол туршилтын текстийн 42-р мөр юм
Энэ бол туршилтын текстийн 43-р мөр юм
Энэ бол туршилтын текстийн 44-р мөр юм
Энэ бол туршилтын текстийн 45-р мөр юм
Энэ бол туршилтын текстийн 46-р мөр юм
Энэ бол туршилтын текстийн 47-р мөр юм
Энэ бол туршилтын текстийн 48-р мөр юм
Энэ бол туршилтын текстийн 49-р мөр юм
Энэ бол туршилтын текстийн 50-р мөр юм</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>Баруун гар горим</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>Зүүн гар</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Хулганы гарыг сонгоно уу</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Хулганы хөдөлгөөн</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Байгалийн гүйлгэх</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Дунд зэргийн эмуляцийг идэвхжүүлсэн</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Баруун гар горим</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Зүүн гар</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Удаан</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Хурдан</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>Хулганы тохиргоо</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">SLow</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Хурдан</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">Зэрлэг</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">Хулгана эсвэл TouchPad Dbus холбосон!</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">анхааруулга</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">Ачаалал qss файл амжилтгүй боллоо!</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Маягт</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>TouchPad идэвхжүүлсэн</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished">SwitchTouchPadEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>TouchPad гарыг сонгоно уу</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished">ComboTouchPadHand</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>TouchPad Motion хурд</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished">SliderTouchPadMotion</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>Удаан</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>Хурдан</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>Арга сонгоно уу</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished">ComboClickMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>Урвах аргыг сонгоно уу</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished">ComboScrollMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>Байгалийн гүйлгэх</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished">ComboNaturalScroll</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>Тайрах үед идэвхждэг</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished">Шилжүүлэгч</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>товш</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished">SwtichTapToClick</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>Баруун гар горим</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>Зүүн гар</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>Хэвлэлийн</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>Та</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>Хоёр хурууны гүйлгэх</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>Edge</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Touchpad идэвхжүүлсэн</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">TouchPad</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">TouchPad идэвхжүүлсэн</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad гарыг сонгоно уу</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad Motion хурд</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">Арга сонгоно уу</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Урвах аргыг сонгоно уу</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Байгалийн гүйлгэх</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Тайрах үед идэвхждэг</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">товш</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Удаан</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Хурдан</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Баруун гар горим</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Зүүн гар</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Хэвлэлийн</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Та</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Хоёр хурууны гүйлгэх</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>TouchPad тохиргоо</translation>
    </message>
</context>
</TS>
