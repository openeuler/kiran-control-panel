<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">Тінтуір және сенсорлық тақта</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Тінтуір қолын таңдау</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Тінтуір қозғалысын үдету</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Табиғи айналдыру</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Ортаңғы эмуляция қосылған</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Сенсорлық тақта қосылды</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad қолын таңдау</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad қозғалысын үдету</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">Әдіс түймешігін басыңыз</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Айналдыру әдісін таңдау</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Теру кезінде қосылған</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">Басу түймешігін түртіңіз</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Ысыру</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Шығу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Болдырмау</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сақтау</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">Тінтуір параметрлері</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">Сенсорлық тақта параметрлері</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң қол режимі</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол жақ қол режімі</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Басу және түрту</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Түрту</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Екі саусақ айналдыру</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge айналдыру</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Баяу</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Іс жүзінде</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Пішін</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>Тінтуір қолын таңдау</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished">ComboSelectMouseHand</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>Тінтуір қозғалысын үдету</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished">CliderMouseMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>Баяу</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>Іс жүзінде</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>Табиғи айналдыру</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished">SwitchMouseNatturalScroll</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>Ортаңғы эмуляция қосылған</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished">SwitchMiddleEmulation</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>Тестмос-Вирлирексин</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>Бұл сынақ мәтінінің 1-жолы
Бұл тест мәтінінің 2-жолы
Бұл тест мәтінінің 3-жолы
Бұл тест мәтінінің 4-жолы
Бұл тест мәтінінің 5-жолы
Бұл тест мәтінінің 6-жолы
Бұл тест мәтінінің 7-жолы
Бұл тест мәтінінің 8-жолы
Бұл тест мәтінінің 9-жолы
Бұл тест мәтінінің 10-жолы
Бұл тест мәтінінің 11-жолы
Бұл тест мәтінінің 12-жолы
Бұл тест мәтінінің 13-жолы
Бұл тест мәтінінің 14-жолы
Бұл тест мәтінінің 15-жолы
Бұл тест мәтінінің 16-жолы
Бұл тест мәтінінің 17-жолы
Бұл тест мәтінінің 18-жолы
Бұл тест мәтінінің 19-жолы
Бұл тест мәтінінің 20-жолы
Бұл тест мәтінінің 21-жолы
Бұл тест мәтінінің 22-жолы
Бұл тест мәтінінің 23-жолы
Бұл тест мәтінінің 24-жолы
Бұл тест мәтінінің 25-жолы
Бұл тест мәтінінің 26-жолы
Бұл тест мәтінінің 27-жолы
Бұл тест мәтінінің 28-жолы
Бұл тест мәтінінің 29-жолы
Бұл тест мәтінінің 30-жолы
Бұл тест мәтінінің 31-жолы
Бұл тест мәтінінің 32-жолы
Бұл тест мәтінінің 33-жолы
Бұл тест мәтінінің 34-жолы
Бұл тест мәтінінің 35-жолы
Бұл тест мәтінінің 36-жолы
Бұл тест мәтінінің 37-жолы
Бұл тест мәтінінің 38-жолы
Бұл тест мәтінінің 39-жолы
Бұл тест мәтінінің 40-жолы
Бұл тест мәтінінің 41-жолы
Бұл тест мәтінінің 42-жолы
Бұл тест мәтінінің 43-жолы
Бұл тест мәтінінің 44-жолы
Бұл тест мәтінінің 45-жолы
Бұл тест мәтінінің 46-жолы
Бұл тест мәтінінің 47-жолы
Бұл тест мәтінінің 48-жолы
Бұл тест мәтінінің 49-жолы
Бұл тест мәтінінің 50-жолы</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>Оң қол режимі</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>Сол жақ қол режімі</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">Тінтуір қолын таңдау</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">Тінтуір қозғалысын үдету</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Табиғи айналдыру</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">Ортаңғы эмуляция қосылған</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң қол режимі</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол жақ қол режімі</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Баяу</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Іс жүзінде</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>Тінтуір параметрлері</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">Слоу</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Іс жүзінде</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">Жаңылыс</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">Тінтуірді немесе TouchPad Dbus бағдарламасын қосу жаңылысы!</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">Ескерту</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">Qss файлын жүктеу жаңылысы!</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Пішін</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>TouchPad қосылған</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished">SwitchTouchPadEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>TouchPad қолын таңдау</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished">ComboTouchPadHand</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>TouchPad қозғалысын үдету</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished">SliderTouchPadMotionAcceleration</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>Баяу</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>Іс жүзінде</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>Әдіс түймешігін басыңыз</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished">ComboClickMethod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>Айналдыру әдісін таңдау</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished">ComboScrollMephod</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>Табиғи айналдыру</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished">ComboNaturalScroll</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>Теру кезінде қосылған</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished">SwitchTypingEnable</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>Басу түймешігін түртіңіз</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished">SwtickTapToClick</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>Оң қол режимі</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>Сол жақ қол режімі</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>Басу және түрту</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>Түрту</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>Екі саусақ айналдыру</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>Edge айналдыру</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">Сенсорлық тақта қосылды</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">TouchPad құрылғысын өшіру</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">TouchPad қосылған</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">TouchPad қолын таңдау</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">TouchPad қозғалысын үдету</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">Әдіс түймешігін басыңыз</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">Айналдыру әдісін таңдау</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">Табиғи айналдыру</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">Теру кезінде қосылған</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">Басу түймешігін түртіңіз</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Баяу</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">Стандарт</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Іс жүзінде</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">Оң қол режимі</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">Сол жақ қол режімі</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">Басу және түрту</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">Түрту</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">Екі саусақ айналдыру</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">Edge айналдыру</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>Сенсорлық тақта параметрлері</translation>
    </message>
</context>
</TS>
