<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranAvatarEditor</name>
    <message>
        <location filename="../src/kiran-avatar-editor.cpp" line="42"/>
        <source>Avatar Editor</source>
        <translation>باش سۈرەت تەھرىرلىگۈچ</translation>
    </message>
    <message>
        <location filename="../src/kiran-avatar-editor.cpp" line="91"/>
        <source>Confirm</source>
        <translation>ئېتىراپ قىلىش</translation>
    </message>
    <message>
        <location filename="../src/kiran-avatar-editor.cpp" line="112"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
</TS>
